package com.dhbw.horbapp;

import com.dhbw.horbapp.models.Attraction;
import com.dhbw.horbapp.models.Event;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Fragment showing an events details
 * @author Bastian Blessing
 *
 */
public class EventFragment extends Fragment {
	private Event event;
	private Attraction attraction;

	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, 
        Bundle savedInstanceState) {
    	
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.event_fragment, container, false);
    }

    @Override
    public void onResume(){
    	super.onResume();

    	fillInData(event,attraction);
    }
    
    /**
     * Fill in data into fragment; only invoke when fragment is displayed
     * @param event that is to be displayed
     * @param attraction of the event
     */
    @SuppressWarnings("deprecation")
	public void fillInData(Event event, Attraction attraction) {
        //Save all data for later on resume
    	this.event = event;
    	this.attraction = attraction;
        		
    	View view = getView();
		
		if(view!=null){	
			//Show event data
			((TextView) view.findViewById(R.id.event_start)).setText(event.getStartTime().toLocaleString());
			((TextView) view.findViewById(R.id.event_end)).setText(event.getEndTime().toLocaleString());
	
			//Show attraction data
			((TextView) view.findViewById(R.id.event_attraction_title)).setText(attraction.getTitle());					
			((TextView) view.findViewById(R.id.event_attraction_short)).setText(attraction.getShortDescription());
		}
    }
}
