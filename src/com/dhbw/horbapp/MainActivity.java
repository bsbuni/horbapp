package com.dhbw.horbapp;

import java.util.List;

import com.dhbw.horbapp.models.Address;
import com.dhbw.horbapp.models.Attraction;
import com.dhbw.horbapp.models.Builder;
import com.dhbw.horbapp.models.Category;
import com.dhbw.horbapp.models.Contact;
import com.dhbw.horbapp.models.Event;
import com.dhbw.horbapp.models.Extrainfo;
import com.dhbw.horbapp.models.Image;
import com.dhbw.horbapp.utils.DataRetrievalHelper;

import android.support.v7.app.ActionBarActivity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

/**
 * Entrance of the application
 * 
 * @author Bastian Blessing
 * 
 */
public class MainActivity extends ActionBarActivity {
	@SuppressWarnings("unused")
	private static final String TAG = "MainActivity";

	private static final int ATTRACTIONS_INDEX = 0;
	private static final int CATEGORY_INDEX = 1;
	private static final int EVENT_INDEX = 2;
	private static final int CONTACT_INDEX = 3;
	private static final int GET_OFFLINE_DATA_INDEX = 4;
	private static final int SWITCH_TO_ONLINE_MODE_INDEX = 5;

	// private static DataProvider dataProvider;
	private DataRetrievalHelper dataRetrievalHelper;

	private String appName;
	private String[] leftDrawerItems;
	private DrawerLayout drawerLayout;
	private ListView leftDrawer;
	private ListView navigationList;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		//Get app name from resources
		appName = getResources().getString(R.string.app_name);

		// Initialize drawer layout
		leftDrawerItems = getResources().getStringArray(
				R.array.left_drawer_items);
		drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		leftDrawer = (ListView) findViewById(R.id.left_drawer);

		// Set the adapter for left drawer list
		leftDrawer.setAdapter(new ArrayAdapter<String>(this,
				R.layout.left_drawer_item, leftDrawerItems));
		// Set the list's click listener
		leftDrawer.setOnItemClickListener(new DrawerItemClickListener());

		dataRetrievalHelper = new DataRetrievalHelper(getApplicationContext());

		FragmentManager fragmentManager = getSupportFragmentManager();
		
		// Remove all fragments from back stack on recreate
		while(fragmentManager.getBackStackEntryCount()>0){
			fragmentManager.popBackStackImmediate();
		}
		
		// Prepare NavigationListFragment on main content
		Fragment fragment = new StartFragment();
				
		fragmentManager.beginTransaction()
				.addToBackStack(appName)
				.replace(R.id.content_frame, fragment).commit();
		fragmentManager.executePendingTransactions();
		
		//Add listener for updating of ui on back click event
		fragmentManager.addOnBackStackChangedListener(
		        new FragmentManager.OnBackStackChangedListener() {
		            public void onBackStackChanged() {
		                //Update title on back navigation
		            	int entries = getSupportFragmentManager().getBackStackEntryCount();
		            	if(entries>0){
			            	String title = getSupportFragmentManager().getBackStackEntryAt(entries-1).getName();
			            	setTitle(title);
		            	}
			            else{
			            	finish();
		            	}
		            }
		        });
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void setTitle(CharSequence title) {
		getSupportActionBar().setTitle(title);
	}

	// Left drawer click handling
	// ----------------------------------------------------------------------------------------------------

	/**
	 * OnClickListener for all items on the left drawer
	 * 
	 * @author Bastian Blessing
	 * 
	 */
	private class DrawerItemClickListener implements
			ListView.OnItemClickListener {
		@Override
		public void onItemClick(
				@SuppressWarnings("rawtypes") AdapterView parent, View view,
				int position, long id) {
			selectDrawerItem(position);
		}
	}

	/**
	 * Handles selection of items on the left drawer
	 * 
	 * @param position
	 *            off selected item on the list
	 */
	private void selectDrawerItem(int position) {
		leftDrawer = (ListView) findViewById(R.id.left_drawer);
		@SuppressWarnings("rawtypes")
		ArrayAdapter navigationListAdapter = null;

		switch (position) {
		case ATTRACTIONS_INDEX: // Attractions
			List<Attraction> attractions = dataRetrievalHelper
					.getAllAttractions();

			if (attractions != null) {// Check whether any data retrieval was
				// successful
				navigationListAdapter = new ArrayAdapter<Attraction>(this,
						R.layout.navigation_item, attractions);
			} else {
				Toast.makeText(
						// Online and offline mode failed
						getApplicationContext(),
						"Das Bereitstellen der Daten schlug fehl. Versuchen sie ggf. in den Online Modus zu wechseln.",
						Toast.LENGTH_LONG).show();
			}

			break;
		case CATEGORY_INDEX: // Categories
			// Get all categories
			List<Category> categories = dataRetrievalHelper.getAllCategories();

			if (categories != null) {// Check whether any data retrieval was
				// successful
				navigationListAdapter = new ArrayAdapter<Category>(this,
						R.layout.navigation_item, categories);
			} else {
				Toast.makeText(
						// Online and offline mode failed
						getApplicationContext(),
						"Das Bereitstellen der Daten schlug fehl. Versuchen sie ggf. in den Online Modus zu wechseln.",
						Toast.LENGTH_LONG).show();
			}

			break;
		case EVENT_INDEX: // Events
			// Get all events
			List<Event> events = dataRetrievalHelper.getAllEvents();

			if (events != null) {// Check whether any data retrieval was
				// successful
				navigationListAdapter = new ArrayAdapter<Event>(this,
						R.layout.navigation_item, events);
			} else {
				Toast.makeText(
						// Online and offline mode failed
						getApplicationContext(),
						"Das Bereitstellen der Daten schlug fehl. Versuchen sie ggf. in den Online Modus zu wechseln.",
						Toast.LENGTH_LONG).show();
			}

			break;
		case CONTACT_INDEX: // Contacts
			// Get all contacts
			List<Contact> contacts = dataRetrievalHelper.getAllContacts();

			if (contacts != null) {// Check whether any data retrieval was
				// successful
				navigationListAdapter = new ArrayAdapter<Contact>(this,
						R.layout.navigation_item, contacts);
			} else {
				Toast.makeText(
						// Online and offline mode failed
						getApplicationContext(),
						"Das Bereitstellen der Daten schlug fehl. Versuchen sie ggf. in den Online Modus zu wechseln.",
						Toast.LENGTH_LONG).show();
			}

			break;
		case GET_OFFLINE_DATA_INDEX: // Get offline data if possible
			if(dataRetrievalHelper.isOnline()==true){//Online mode
				if(dataRetrievalHelper.getOfflineData()==true){
					Toast.makeText(
							getApplicationContext(),
							"Daten stehen nun offline zur Verf�gung.",
							Toast.LENGTH_LONG).show();
				}
				else{
					Toast.makeText(
							getApplicationContext(),
							"Daten konnten leider nicht bereitgestellt werden.",
							Toast.LENGTH_LONG).show();
				}
			}
			else{//Offline mode or no data available
				Toast.makeText(
						getApplicationContext(),
						"Daten k�nnen nur im Online Modus verf�gbar gemacht werden. Versuchen sie ggf. in den Online Modus zu wechseln.",
						Toast.LENGTH_LONG).show();
			}
			break;
		case SWITCH_TO_ONLINE_MODE_INDEX: // Switch to online mode if possible

			if(dataRetrievalHelper.switchToOnlineMode() == true){//Switch to online mode successful
				//Move back to the first fragment
				FragmentManager fragmentManager = getSupportFragmentManager();
				while(fragmentManager.getBackStackEntryCount()>1){
					fragmentManager.popBackStackImmediate();
				}
			}
		}

		if (position != GET_OFFLINE_DATA_INDEX
				&& position != SWITCH_TO_ONLINE_MODE_INDEX
				&& navigationListAdapter != null) {// Just buttons with navigation function
			// Prepare NavigationListFragment on main content
			NavigationFragment fragment = new NavigationFragment();
			
			//Remove previous fragments from other subnavigations
			FragmentManager fragmentManager = getSupportFragmentManager();
			while(fragmentManager.getBackStackEntryCount()>1){
				fragmentManager.popBackStackImmediate();
			}
			
			// Insert the fragment by replacing any existing fragment
			fragmentManager.beginTransaction()
					.addToBackStack(leftDrawerItems[position])
					.replace(R.id.content_frame, fragment).commit();
			fragmentManager.executePendingTransactions();
			
			//Fill data into fragment
			fragment.fillInData(navigationListAdapter, new NavigationItemClickListener(
					position));

			// Highlight the selected item
			leftDrawer.setItemChecked(position, true);
			// Update title
			setTitle(leftDrawerItems[position]);
		}

		// Close left drawer
		drawerLayout.closeDrawer(leftDrawer);
	}

	// Navigation lists click handling
	// ------------------------------------------------------------------------------

	/**
	 * OnClickListener for all items on the navigation lists
	 * 
	 * @author Bastian Blessing
	 * 
	 */
	public class NavigationItemClickListener implements
			ListView.OnItemClickListener {
		int leftDrawerPosition;

		/**
		 * Constructor of the NavigationItemClickListener
		 * 
		 * @param leftDrawerPosition
		 *            indicates the navigation list which is selected
		 */
		public NavigationItemClickListener(int leftDrawerPosition) {
			// Indicates the subnavigation in which the navigation list is set
			// (attractions, categories, ...)
			this.leftDrawerPosition = leftDrawerPosition;
		}

		@Override
		public void onItemClick(
				@SuppressWarnings("rawtypes") AdapterView parent, View view,
				int position, long id) {
			selectNavigationItem(leftDrawerPosition, position);
		}
	}

	/**
	 * Handles selection of items on the left drawer
	 * 
	 * @param leftDrawerPosition
	 *            indicates the navigation list which is selected
	 * @param position
	 *            off selected item on the list
	 */
	private void selectNavigationItem(int leftDrawerPosition, int position) {
		Fragment fragment = null;
		String title = "";
		navigationList = (ListView) findViewById(R.id.navigation_list);

		switch (leftDrawerPosition) {
		case ATTRACTIONS_INDEX: // Attractions
			// Get selected item from navigation list
			Attraction attraction = (Attraction) navigationList.getAdapter()
					.getItem(position);
			int attractionId = attraction.getServerId();
			
			//Get optional data
			Builder attractionBuilder = dataRetrievalHelper.getBuilderByAttraction(attractionId);
			Extrainfo attractionExtrainfo = dataRetrievalHelper.getExtrainfoByAttraction(attractionId);
			Address attractionAddress = dataRetrievalHelper.getAddressByAttraction(attractionId);
			List<Image> attractionImages = dataRetrievalHelper.getAllImagesByAttraction(attractionId);
			
			// Create a new fragment and insert attraction data
			AttractionFragment attractionFragment = new AttractionFragment();
			attractionFragment.fillInData(attraction,
					attractionBuilder, attractionExtrainfo, attractionAddress, attractionImages);
			
			fragment = attractionFragment;
			
			// Prepare title
			title = attraction.toString();

			break;
		case CATEGORY_INDEX: // Categories - ! fragment has to stay null
			// Get selected item from navigation list
			Category category = (Category) navigationList.getAdapter().getItem(
					position);
			
			//Get category of attractions
			List<Attraction> categoryAttractions = dataRetrievalHelper.getAllAttractionsByCategory(category.getServerId());

			if(categoryAttractions!=null){//Data retrieval successful				
				// Create a new fragment and insert attractions of category
				NavigationFragment navFragment = new NavigationFragment();

				// Insert the fragment by replacing any existing fragment
				FragmentManager fragmentManager = getSupportFragmentManager();
				
				fragmentManager.beginTransaction()
				.addToBackStack(category.toString())
				.replace(R.id.content_frame, navFragment).commit();
				fragmentManager.executePendingTransactions();

				//Fill data into fragment
				navFragment.fillInData(new ArrayAdapter<Attraction>(this,
						R.layout.navigation_item, categoryAttractions),new NavigationItemClickListener(ATTRACTIONS_INDEX));
			}

			break;
		case EVENT_INDEX: // Events
			// Get selected item from navigation list
			Event event = (Event) navigationList.getAdapter().getItem(position);
			
			// Get attraction of event
			Attraction eventAttraction = dataRetrievalHelper.getAttractionByEvent(event.getServerId());

			if(eventAttraction!=null){//Data retrieval successful				
				// Create a new fragment and insert event data
				EventFragment eventFragment = new EventFragment();
				eventFragment.fillInData(event, eventAttraction);
				
				fragment = eventFragment;
				
				// Prepare title
				title = event.toString();
			}

			break;
		case CONTACT_INDEX: // Contacts
			// Get selected item from navigation list
			Contact contact = (Contact) navigationList.getAdapter().getItem(
					position);

			//Get optional data
			Address contactAddress = dataRetrievalHelper.getAddressByContact(contact.getServerId());

			// Create a new fragment and insert contact data
			ContactFragment contactFragment = new ContactFragment();
			contactFragment.fillInData(contact, contactAddress);

			fragment = contactFragment;
			
			// Prepare title
			title = contact.toString();

			break;
		}

		if(fragment!=null){//All data retrieval was successful
			// Update title
			setTitle(title);
	
			// Insert the fragment by replacing any existing fragment
			FragmentManager fragmentManager = getSupportFragmentManager();
			
			fragmentManager.beginTransaction()
					.addToBackStack(title)
					.replace(R.id.content_frame, fragment).commit();
			fragmentManager.executePendingTransactions();
		}
	}
}
