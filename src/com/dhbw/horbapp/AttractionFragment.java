package com.dhbw.horbapp;

import java.util.List;

import com.dhbw.horbapp.models.Address;
import com.dhbw.horbapp.models.Attraction;
import com.dhbw.horbapp.models.Builder;
import com.dhbw.horbapp.models.Extrainfo;
import com.dhbw.horbapp.models.Image;

import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Fragment showing an attractions details
 * @author Bastian Blessing
 *
 */
public class AttractionFragment extends Fragment {
	private Attraction attraction;
	private Builder builder;
	private Extrainfo extrainfo;
	private Address address;
	private List<Image> images;
	private int imageIndex = 0;
	
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, 
        Bundle savedInstanceState) {
    	
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.attraction_fragment, container, false);
    }

    @Override
    public void onResume(){
    	super.onResume();

    	fillInData(attraction,builder,extrainfo,address,images);
	}
    
    /**
     * Fill in data into fragment; only invoke when fragment is displayer
     * @param attraction that is to be displayed
     * @param builder of the attraction; can be null
     * @param extrainfo of the attraction; can be null
     * @param address of the attraction; can be null
     * @param images of the attraction; can be null or empty
     */
    @SuppressWarnings("deprecation")
	public void fillInData(Attraction attraction, Builder builder, Extrainfo extrainfo, Address address, List<Image> images) {
    	//Save all data for later on resume
    	this.attraction = attraction;
    	this.builder = builder;
    	this.extrainfo = extrainfo;
    	this.address = address;
    	this.images = images;

    	View view = getView();
    	
    	if(view!=null){				
			//Show attraction data
			((TextView) view.findViewById(R.id.attraction_short_description)).setText(attraction.getShortDescription());
			((TextView) view.findViewById(R.id.attraction_description)).setText(attraction.getDescription());					
			
			
			//Add images if available
	        if(images!=null && !images.isEmpty()){//Images for attraction available
	        	view.findViewById(R.id.attraction_image_container).setVisibility(View.VISIBLE);	
	        	
	        	//Show first image
	        	setImage(0); 
	        	
	        	if(images.size()>1){//More than one picture
	        		//Add buttons for navigation
	        		Button prevButton = (Button) view.findViewById(R.id.attraction_image_previous);
	        		Button nextButton = (Button) view.findViewById(R.id.attraction_image_next);
	        		
	        		prevButton.setVisibility(View.VISIBLE);
	        		nextButton.setVisibility(View.VISIBLE);
	        		
	        		//Finalize value for listener
	        		final int imagesSize = images.size();
	        		
	        		//Handle click on previous button
	        		prevButton.setOnClickListener(new OnClickListener(){
	
						@Override
						public void onClick(View v) {
							int index;
							if(imageIndex <= 0){//First picture
								//Navigate to last picture
								index = imagesSize-1;
							}
							else{//Not first picture
								//Navigate to previous picture
								index = imageIndex - 1;
							}
							
							setImage(index);
						}
	        		});
	        		
	        		//Handle click on next button
	        		nextButton.setOnClickListener(new OnClickListener(){
	
						@Override
						public void onClick(View v) {
							int index;
							if(imageIndex >= imagesSize-1){//Last picture
								//Navigate to first picture
								index = 0;
							}
							else{//Not last picture
								//Navigate to next picture
								index = imageIndex + 1;
							}
							
							setImage(index);
						}
	        		});
	    		}
	        }
	    	
	    	//Show builder data if available
	    	if(builder!=null && builder.getName()!=null){
	    		((TextView) view.findViewById(R.id.attraction_builder_name)).setText(builder.getName());
	    		view.findViewById(R.id.attraction_builder_name_container).setVisibility(View.VISIBLE);
	    		
	    		((TextView) view.findViewById(R.id.attraction_builder_birth)).setText(builder.getBirthDate().toLocaleString());
	    		view.findViewById(R.id.attraction_builder_birth_container).setVisibility(View.VISIBLE);
	    		
	    		((TextView) view.findViewById(R.id.attraction_builder_death)).setText(builder.getDeathDate().toLocaleString());
	    		view.findViewById(R.id.attraction_builder_death_container).setVisibility(View.VISIBLE);
	    	}
	    	
	    	//Show extrainfo data if available
	    	if(extrainfo!=null && extrainfo.getOpeningHours()!=null){
	    		((TextView) view.findViewById(R.id.attraction_extrainfo_price)).setText(extrainfo.getPrice());
	    		view.findViewById(R.id.attraction_extrainfo_price_container).setVisibility(View.VISIBLE);
	    		
	    		((TextView) view.findViewById(R.id.attraction_extrainfo_opening)).setText(extrainfo.getOpeningHours());
	    		view.findViewById(R.id.attraction_extrainfo_opening_container).setVisibility(View.VISIBLE);
	    	}
	    	
	    	//Show address data if available
	    	if(address!=null && address.getStreet()!=null){
	    		((TextView) view.findViewById(R.id.attraction_address_street)).setText(address.getStreet() + " " + address.getStreetnumber());
	    		view.findViewById(R.id.attraction_address_street_container).setVisibility(View.VISIBLE);
	    		
	    		((TextView) view.findViewById(R.id.attraction_address_city)).setText(address.getPostCode() + " " + address.getCity());
	    		view.findViewById(R.id.attraction_address_city_container).setVisibility(View.VISIBLE);
	    	}
		}
    }
    
    /**
     * Sets image to the fragments ImageView
     * @param index of the image in the list of images
     */
    private void setImage(int index){
    	Image image = images.get(index);
    	
    	ImageView imageView = (ImageView) getView().findViewById(R.id.attraction_image);
		imageView.setImageBitmap(BitmapFactory.decodeByteArray( image.getBlob(), 
		        0,image.getBlob().length));
    	this.imageIndex = index;
    }
}
