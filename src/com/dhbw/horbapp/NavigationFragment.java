package com.dhbw.horbapp;

import com.dhbw.horbapp.MainActivity.NavigationItemClickListener;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

/**
 * Fragment showing a navigation list for attractions, categories, events or contacts
 * @author Bastian Blessing
 *
 */
public class NavigationFragment extends Fragment {
    
	@SuppressWarnings("rawtypes")
	private ArrayAdapter adapter;
	private NavigationItemClickListener listener;
	private ListView navigationList;
	
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, 
        Bundle savedInstanceState) {
    	
        // Inflate the layout for this fragment
    	return inflater.inflate(R.layout.navigation_fragment, container, false);
    }
    
    @Override
    public void onResume(){
    	super.onResume();
    	fillInData(adapter, listener);
    }
    
    /**
     * Fill in data into fragment; only invoke when fragment is displayed
     * @param adapter for listview content
     * @param listener for listview click events
     */
	public void fillInData(@SuppressWarnings("rawtypes") ArrayAdapter adapter, NavigationItemClickListener listener){
		this.adapter = adapter;
		this.listener = listener;
		
		View view = getView();
		
		if(view!=null){			
			navigationList = (ListView) view.findViewById(R.id.navigation_list);
			// Set the adapter for navigation list
			navigationList.setAdapter(adapter);
			navigationList.setOnItemClickListener(listener);
		}
	}
}
