package com.dhbw.horbapp.utils;

import java.io.IOException;
import java.util.List;

import com.dhbw.horbapp.models.Address;
import com.dhbw.horbapp.models.Attraction;
import com.dhbw.horbapp.models.Builder;
import com.dhbw.horbapp.models.Category;
import com.dhbw.horbapp.models.Contact;
import com.dhbw.horbapp.models.Event;
import com.dhbw.horbapp.models.Extrainfo;
import com.dhbw.horbapp.models.Image;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

/**
 * Class for access of data from online or offline data source handling all data providing errors
 * @author Bastian Blessing
 *
 */
public class DataRetrievalHelper implements DataProvider{
	private static final String TAG = "DataRetrievalHelper";
	private static DataProvider dataProvider;
	private Context context;
	
	public DataRetrievalHelper(Context context){
		this.context = context;
		switchToOnlineMode();
	}
	
	/**
	 * Retrieves all application data from server and writes it to the
	 * underlying database
	 * S
	 * @return true if data retrieval was successfull; false if not
	 */
	public boolean getOfflineData() {
		return OfflineDataProvider.getOfflineData(context);
	}
	
	/**
	 * Switches application to online mode if possible
	 * @return true if switch was successfull; false if offline mode was possible; null else
	 */
	public Boolean switchToOnlineMode(){
		getOptimalDataProvider();
		
		if(dataProvider == null){
			Toast.makeText(//Online and offline mode failed
					context,
					"Das Bereitstellen der Daten schlug fehl. Versuchen sie ggf. in den Online Modus zu wechseln.",
					Toast.LENGTH_LONG).show();
			return null;
		}
		else if(!dataProvider.isOnline()){
			Toast.makeText(//Online mode failed
					context,
					"Das Bereitstellen der Daten vom Server schlug fehl. Die App befindet sich nun im Offline-Modus.",
					Toast.LENGTH_LONG).show();
			return false;
		}
		else{
			Toast.makeText(//Switch  to online mode successful
					context,
					"Die App ist im Online-Modus.",
					Toast.LENGTH_LONG).show();
			return true;
		}
	}
	
	/**
	 * Sets optimal DataProvider; online > offline > none (null)
	 * @param context of program execution
	 */
	private void getOptimalDataProvider(){		
		//Try to connect to server
		try{
			dataProvider = new OnlineDataProvider();
		}
		catch(IOException e){
			Log.e(TAG,e.getMessage());
			
			//Try to get offline data
			try{
				dataProvider = new OfflineDataProvider(context);
			}
			catch(IOException ie){
				Log.e(TAG,ie.getMessage());
				dataProvider = null;
			}
		}		
	}
	
	
	/**
	 * Is to be invoked when data retrieval from a provider fails; method sets new dataProvider if possible;
	 * IMPORTANT: This method relys on the fact, that every optional data belongs to a non optional data and so the data retrieval has to fail in another step
	 * @param dataProvider the dataProvider until now
	 * @param context of program execution
	 * @param isOptional indicates whether the data that failed to be retrieved is optional
	 */
	private static void onDataRetrievalFail(DataProvider dataProvider, Context context, boolean isOptional){
		Log.e(TAG,"Data error: Data retrieval failed.");
		// If online mode failed and offline mode is available return new dataProvider
		if (dataProvider.isOnline()) {
			try{
				DataRetrievalHelper.dataProvider = new OfflineDataProvider(context);
			}
			catch(IOException ie){
				Log.e(TAG,ie.getMessage());
				DataRetrievalHelper.dataProvider = null;
			}
		} 
		else if(!isOptional){// Online and Offline mode failed
			Log.e(TAG,"Data error: Online and offline mode failed.");
			DataRetrievalHelper.dataProvider = null;
		}	
	}
	
	// Data retrieval methods -----------------------------------------------------------------------------------------------------
	
	@Override
	public boolean isOnline() {
		return dataProvider == null ? false : dataProvider.isOnline();
	}

	@Override
	public List<Category> getAllCategories() {
		List<Category> categories = dataProvider.getAllCategories();

		if (categories == null) {// Data retrieval failed
			// If online mode failed and offline mode is available proceed with
			// programm execution
			onDataRetrievalFail(dataProvider, context, false);
			if (dataProvider != null) {
				Toast.makeText(
						context,
						"Das Bereitstellen der Daten vom Server schlug fehl. Die App befindet sich nun im Offline-Modus.",
						Toast.LENGTH_LONG).show();
				
				categories = dataProvider.getAllCategories();
			} else {//Online and offline mode failed
				Toast.makeText(
						context,
						"Das Bereitstellen der Daten schlug fehl. Versuchen sie ggf. in den Online Modus zu wechseln.",
						Toast.LENGTH_LONG).show();
			}
		}

		return categories;
	}

	@Override
	public List<Attraction> getAllAttractions() {
		List<Attraction> attractions = dataProvider.getAllAttractions();

		if (attractions == null) {// Data retrieval failed
			// If online mode failed and offline mode is available proceed with
			// programm execution
			onDataRetrievalFail(dataProvider, context, false);
			if (dataProvider != null) {
				Toast.makeText(
						context,
						"Das Bereitstellen der Daten vom Server schlug fehl. Die App befindet sich nun im Offline-Modus.",
						Toast.LENGTH_LONG).show();
				
				attractions = dataProvider.getAllAttractions();
			} else {//Online and offline mode failed
				Toast.makeText(
						context,
						"Das Bereitstellen der Daten schlug fehl. Versuchen sie ggf. in den Online Modus zu wechseln.",
						Toast.LENGTH_LONG).show();
			}
		}

		return attractions;
	}
	
	@Override
	public List<Event> getAllEvents() {
		List<Event> events = dataProvider.getAllEvents();

		if (events == null) {// Data retrieval failed
			// If online mode failed and offline mode is available proceed with
			// programm execution
			onDataRetrievalFail(dataProvider, context, false);
			if (dataProvider != null) {
				Toast.makeText(
						context,
						"Das Bereitstellen der Daten vom Server schlug fehl. Die App befindet sich nun im Offline-Modus.",
						Toast.LENGTH_LONG).show();
				
				events = dataProvider.getAllEvents();
			} else {//Online and offline mode failed
				Toast.makeText(
						context,
						"Das Bereitstellen der Daten schlug fehl. Versuchen sie ggf. in den Online Modus zu wechseln.",
						Toast.LENGTH_LONG).show();
			}
		}

		return events;
	}

	@Override
	public List<Contact> getAllContacts() {
		List<Contact> contacts = dataProvider.getAllContacts();

		if (contacts == null) {// Data retrieval failed
			// If online mode failed and offline mode is available proceed with
			// programm execution
			onDataRetrievalFail(dataProvider, context, false);
			if (dataProvider != null) {
				Toast.makeText(
						context,
						"Das Bereitstellen der Daten vom Server schlug fehl. Die App befindet sich nun im Offline-Modus.",
						Toast.LENGTH_LONG).show();
				
				contacts = dataProvider.getAllContacts();
			} else {//Online and offline mode failed
				Toast.makeText(
						context,
						"Das Bereitstellen der Daten schlug fehl. Versuchen sie ggf. in den Online Modus zu wechseln.",
						Toast.LENGTH_LONG).show();
			}
		}

		return contacts;
	}

	@Override
	public List<Attraction> getAllAttractionsByCategory(int categoryId) {
		List<Attraction> attractions = dataProvider.getAllAttractionsByCategory(categoryId);

		if (attractions == null) {// Data retrieval failed
			// If online mode failed and offline mode is available proceed with
			// programm execution
			onDataRetrievalFail(dataProvider, context, false);
			if (dataProvider != null) {
				Toast.makeText(
						context,
						"Das Bereitstellen der Daten vom Server schlug fehl. Die App befindet sich nun im Offline-Modus.",
						Toast.LENGTH_LONG).show();
				
				attractions = dataProvider.getAllAttractionsByCategory(categoryId);
			} else {//Online and offline mode failed
				Toast.makeText(
						context,
						"Das Bereitstellen der Daten schlug fehl. Versuchen sie ggf. in den Online Modus zu wechseln.",
						Toast.LENGTH_LONG).show();
			}
		}

		return attractions;
	}

	@Override
	public Builder getBuilderByAttraction(int attractionId) {
		Builder builder = dataProvider.getBuilderByAttraction(attractionId);

		if (builder == null) {// Data retrieval failed
			// If online mode failed and offline mode is available proceed with
			// programm execution
			onDataRetrievalFail(dataProvider, context, true);
			if (dataProvider != null) {
				Toast.makeText(
						context,
						"Das Bereitstellen der Daten vom Server schlug fehl. Die App befindet sich nun im Offline-Modus.",
						Toast.LENGTH_LONG).show();
				
				builder = dataProvider.getBuilderByAttraction(attractionId);
			} else {//Online and offline mode failed
				Toast.makeText(
						context,
						"Das Bereitstellen der Daten schlug fehl. Versuchen sie ggf. in den Online Modus zu wechseln.",
						Toast.LENGTH_LONG).show();
			}
		}

		return builder;
	}

	@Override
	public Extrainfo getExtrainfoByAttraction(int attractionId) {
		Extrainfo extrainfo = dataProvider.getExtrainfoByAttraction(attractionId);

		if (extrainfo == null) {// Data retrieval failed
			// If online mode failed and offline mode is available proceed with
			// programm execution
			onDataRetrievalFail(dataProvider, context, true);
			if (dataProvider != null) {
				Toast.makeText(
						context,
						"Das Bereitstellen der Daten vom Server schlug fehl. Die App befindet sich nun im Offline-Modus.",
						Toast.LENGTH_LONG).show();
				
				extrainfo = dataProvider.getExtrainfoByAttraction(attractionId);
			} else {//Online and offline mode failed
				Toast.makeText(
						context,
						"Das Bereitstellen der Daten schlug fehl. Versuchen sie ggf. in den Online Modus zu wechseln.",
						Toast.LENGTH_LONG).show();
			}
		}

		return extrainfo;
	}

	@Override
	public List<Image> getAllImagesByAttraction(int attractionId) {
		List<Image> images = dataProvider.getAllImagesByAttraction(attractionId);

		if (images == null) {// Data retrieval failed
			// If online mode failed and offline mode is available proceed with
			// programm execution
			onDataRetrievalFail(dataProvider, context, true);
			if (dataProvider != null) {
				Toast.makeText(
						context,
						"Das Bereitstellen der Daten vom Server schlug fehl. Die App befindet sich nun im Offline-Modus.",
						Toast.LENGTH_LONG).show();
				
				images = dataProvider.getAllImagesByAttraction(attractionId);
			} else {//Online and offline mode failed
				Toast.makeText(
						context,
						"Das Bereitstellen der Daten schlug fehl. Versuchen sie ggf. in den Online Modus zu wechseln.",
						Toast.LENGTH_LONG).show();
			}
		}

		return images;
	}

	@Override
	public Address getAddressByAttraction(int attractionId) {
		Address address = dataProvider.getAddressByAttraction(attractionId);

		if (address == null) {// Data retrieval failed
			// If online mode failed and offline mode is available proceed with
			// programm execution
			onDataRetrievalFail(dataProvider, context, true);
			if (dataProvider != null) {
				Toast.makeText(
						context,
						"Das Bereitstellen der Daten vom Server schlug fehl. Die App befindet sich nun im Offline-Modus.",
						Toast.LENGTH_LONG).show();
				
				address = dataProvider.getAddressByAttraction(attractionId);
			} else {//Online and offline mode failed
				Toast.makeText(
						context,
						"Das Bereitstellen der Daten schlug fehl. Versuchen sie ggf. in den Online Modus zu wechseln.",
						Toast.LENGTH_LONG).show();
			}
		}

		return address;
	}

	@Override
	public Address getAddressByContact(int contactId) {
		Address address = dataProvider.getAddressByContact(contactId);

		if (address == null) {// Data retrieval failed
			// If online mode failed and offline mode is available proceed with
			// programm execution
			onDataRetrievalFail(dataProvider, context, true);
			if (dataProvider != null) {
				Toast.makeText(
						context,
						"Das Bereitstellen der Daten vom Server schlug fehl. Die App befindet sich nun im Offline-Modus.",
						Toast.LENGTH_LONG).show();
				
				address = dataProvider.getAddressByContact(contactId);
			} else {//Online and offline mode failed
				Toast.makeText(
						context,
						"Das Bereitstellen der Daten schlug fehl. Versuchen sie ggf. in den Online Modus zu wechseln.",
						Toast.LENGTH_LONG).show();
			}
		}

		return address;
	}

	@Override
	public Attraction getAttractionByEvent(int eventId) {
		Attraction attraction = dataProvider.getAttractionByEvent(eventId);

		if (attraction == null) {// Data retrieval failed
			// If online mode failed and offline mode is available proceed with
			// programm execution
			onDataRetrievalFail(dataProvider, context, false);
			if (dataProvider != null) {
				Toast.makeText(
						context,
						"Das Bereitstellen der Daten vom Server schlug fehl. Die App befindet sich nun im Offline-Modus.",
						Toast.LENGTH_LONG).show();
				
				attraction = dataProvider.getAttractionByEvent(eventId);
			} else {//Online and offline mode failed
				Toast.makeText(
						context,
						"Das Bereitstellen der Daten schlug fehl. Versuchen sie ggf. in den Online Modus zu wechseln.",
						Toast.LENGTH_LONG).show();
			}
		}

		return attraction;
	}	
}
