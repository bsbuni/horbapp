package com.dhbw.horbapp.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.dhbw.horbapp.models.Address;
import com.dhbw.horbapp.models.Attraction;
import com.dhbw.horbapp.models.Builder;
import com.dhbw.horbapp.models.Category;
import com.dhbw.horbapp.models.Contact;
import com.dhbw.horbapp.models.Event;
import com.dhbw.horbapp.models.Extrainfo;
import com.dhbw.horbapp.models.Image;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Helper class for administration of and access to underlying SQLite database; Android OS will 
 * administrate the database by itself, because of extension of the SQLiteOpenHelper
 * @author bastian.blessing
 *
 */
public class DatabaseHelper extends SQLiteOpenHelper {

	// Logcat tag
	private static final String LOG = "DatabaseHelper";
	// Database Version
	private static final int DATABASE_VERSION = 1;
	// Database Name
	private static final String DATABASE_NAME = "horbAppDatabase";

	// Table Names
	private static final String TABLE_CATEGORY = "category";
	private static final String TABLE_ATTRACTION = "attraction";
	private static final String TABLE_CATEGORY_ATTRACTION = "category_attraction";
	private static final String TABLE_EVENT = "event";
	private static final String TABLE_BUILDER = "builder";
	private static final String TABLE_EXTRAINFO = "extrainfo";
	private static final String TABLE_IMAGE = "image";
	private static final String TABLE_ATTRACTION_IMAGE = "attraction_image";
	private static final String TABLE_CONTACT = "contact";
	private static final String TABLE_ADDRESS = "address";

	// Common column names
	private static final String KEY_ID = "id";
	private static final String KEY_SERVER_ID = "server_id";

	// CATEGORY Table - column names
	private static final String KEY_CATEGORY_NAME = "name";
	private static final String KEY_CATEGORY_DESCRIPTION = "description";

	// ATTRACTION Table - column names
	private static final String KEY_ATTRACTION_TITLE = "title";
	private static final String KEY_ATTRACTION_LONGITUDE = "longitude";
	private static final String KEY_ATTRACTION_LATITUDE = "latitude";
	private static final String KEY_ATTRACTION_PUBLISHED = "published";
	private static final String KEY_ATTRACTION_SHORT_DESCRIPTION = "short_description";
	private static final String KEY_ATTRACTION_DESCRIPTION = "description";
	private static final String KEY_ATTRACTION_INFO = "info";
	private static final String KEY_ATTRACTION_ADDRESS_ID = "address_id";
	private static final String KEY_ATTRACTION_EXTRAINFO_ID = "extrainfo_id";
	private static final String KEY_ATTRACTION_BUILDER_ID = "builder_id";	

	// CATEGORY_ATTRACTION Table - column names
	private static final String KEY_CA_CATEGORY_ID = "category_id";
	private static final String KEY_CA_ATTRACTION_ID = "attraction_id";

	// EVENT Table - column names
	private static final String KEY_EVENT_ATTRACTION_ID = "attraction_id";
	private static final String KEY_EVENT_START_TIME = "start_time";
	private static final String KEY_EVENT_END_TIME = "end_time";
	private static final String KEY_EVENT_CONTENT = "content";

	// BUILDER Table - column names
	private static final String KEY_BUILDER_NAME = "name";
	private static final String KEY_BUILDER_BIRTH_DATE = "birth_date";
	private static final String KEY_BUILDER_DEATH_DATE = "death_date";

	// EXTRAINFO Table - column names
	private static final String KEY_EXTRAINFO_OPENING_HOURS = "opening_hours";
	private static final String KEY_EXTRAINFO_PRICE = "price";

	// IMAGE Table - column names
	private static final String KEY_IMAGE_BLOB = "blob";
	private static final String KEY_IMAGE_PATH = "path";

	// ATTRACTION_IMAGE Table - column names
	private static final String KEY_AI_ATTRACTION_ID = "attraction_id";
	private static final String KEY_AI_IMAGE_ID = "image_id";

	// CONTACT Table - column names
	private static final String KEY_CONTACT_FIRSTNAME = "firstname";
	private static final String KEY_CONTACT_LASTNAME = "lastname";
	private static final String KEY_CONTACT_EMAIL = "email";
	private static final String KEY_CONTACT_OFFICE = "office";
	private static final String KEY_CONTACT_SECTION = "section";
	private static final String KEY_CONTACT_PHONE = "phone";
	private static final String KEY_CONTACT_FAX = "fax";
	private static final String KEY_CONTACT_ADDRESS_ID = "address_id";

	// ADDRESS Table - column names
	private static final String KEY_ADDRESS_POSTCODE = "postcode";
	private static final String KEY_ADDRESS_CITY = "city";
	private static final String KEY_ADDRESS_STREET = "street";
	private static final String KEY_ADDRESS_STREETNUMBER = "streetnumber";

	// Table Create Statements
	// CATEGORY table create statement
	private static final String CREATE_TABLE_CATEGORY = "CREATE TABLE "
			+ TABLE_CATEGORY + "(" + KEY_ID + " INTEGER PRIMARY KEY,"
			+ KEY_SERVER_ID + " INTEGER,"
			+ KEY_CATEGORY_NAME + " TEXT," + KEY_CATEGORY_DESCRIPTION + " TEXT"
			+ ")";

	// Table Create Statements
	// ATTRACTION table create statement
	private static final String CREATE_TABLE_ATTRACTION = "CREATE TABLE "
			+ TABLE_ATTRACTION + "(" + KEY_ID + " INTEGER PRIMARY KEY,"
			+ KEY_SERVER_ID + " INTEGER,"
			+ KEY_ATTRACTION_TITLE + " TEXT," + KEY_ATTRACTION_LONGITUDE
			+ " REAL," + KEY_ATTRACTION_LATITUDE + " REAL,"
			+ KEY_ATTRACTION_PUBLISHED + " BOOLEAN,"
			+ KEY_ATTRACTION_SHORT_DESCRIPTION + " TEXT,"
			+ KEY_ATTRACTION_DESCRIPTION + " TEXT," + KEY_ATTRACTION_INFO
			+ " TEXT," + KEY_ATTRACTION_ADDRESS_ID + " INTEGER,"
			+ KEY_ATTRACTION_BUILDER_ID + " INTEGER,"
			+ KEY_ATTRACTION_EXTRAINFO_ID + " INTEGER" + ")";

	// Table Create Statements
	// CATEGORY_ATTRACTION table create statement
	private static final String CREATE_TABLE_CATEGORY_ATTRACTION = "CREATE TABLE " + TABLE_CATEGORY_ATTRACTION + "("
			+ KEY_ID + " INTEGER PRIMARY KEY," 
			+ KEY_CA_ATTRACTION_ID + " INTEGER,"
			+ KEY_CA_CATEGORY_ID + " INTEGER" + ")";

	// Table Create Statements
	// EVENT table create statement
	private static final String CREATE_TABLE_EVENT = "CREATE TABLE " + TABLE_EVENT + "("
			+ KEY_ID + " INTEGER PRIMARY KEY," 
			+ KEY_SERVER_ID + " INTEGER,"
			+ KEY_EVENT_ATTRACTION_ID + " INTEGER,"
			+ KEY_EVENT_CONTENT + " TEXT," 
			+ KEY_EVENT_START_TIME + " REAL,"
			+ KEY_EVENT_END_TIME + " REAL" + ")";
		
	// Table Create Statements
	// BUILDER table create statement
	private static final String CREATE_TABLE_BUILDER = "CREATE TABLE " + TABLE_BUILDER + "("
			+ KEY_ID + " INTEGER PRIMARY KEY,"
			+ KEY_SERVER_ID + " INTEGER,"
			+ KEY_BUILDER_NAME + " TEXT,"
			+ KEY_BUILDER_BIRTH_DATE + " REAL," 
			+ KEY_BUILDER_DEATH_DATE + " REAL" + ")";

	// Table Create Statements
	// EXTRAINFO table create statement
	private static final String CREATE_TABLE_EXTRAINFO = "CREATE TABLE " + TABLE_EXTRAINFO + "("
			+ KEY_ID + " INTEGER PRIMARY KEY," 
			+ KEY_SERVER_ID + " INTEGER,"
			+ KEY_EXTRAINFO_OPENING_HOURS + " TEXT,"
			+ KEY_EXTRAINFO_PRICE + " TEXT" + ")";

	// Table Create Statements
	// IMAGE table create statement
	private static final String CREATE_TABLE_IMAGE = "CREATE TABLE " + TABLE_IMAGE + "("
			+ KEY_ID + " INTEGER PRIMARY KEY," 
			+ KEY_SERVER_ID + " INTEGER,"
			+ KEY_IMAGE_PATH + " TEXT,"
			+ KEY_IMAGE_BLOB + " BLOB" + ")";

	// Table Create Statements
	// ATTRACTION_IMAGE table create statement
	private static final String CREATE_TABLE_ATTRACTION_IMAGE = "CREATE TABLE " + TABLE_ATTRACTION_IMAGE + "("
			+ KEY_ID + " INTEGER PRIMARY KEY," 
			+ KEY_AI_ATTRACTION_ID + " INTEGER,"
			+ KEY_AI_IMAGE_ID + " INTEGER" + ")";

	// Table Create Statements
	// CONTACT table create statement
	private static final String CREATE_TABLE_CONTACT = "CREATE TABLE " + TABLE_CONTACT + "("
			+ KEY_ID + " INTEGER PRIMARY KEY," 
			+ KEY_SERVER_ID + " INTEGER,"
			+ KEY_CONTACT_ADDRESS_ID + " INTEGER,"
			+ KEY_CONTACT_FAX + " TEXT,"
			+ KEY_CONTACT_PHONE + " TEXT,"
			+ KEY_CONTACT_SECTION + " TEXT,"
			+ KEY_CONTACT_OFFICE + " TEXT,"
			+ KEY_CONTACT_EMAIL + " TEXT," 
			+ KEY_CONTACT_FIRSTNAME + " TEXT,"
			+ KEY_CONTACT_LASTNAME + " TEXT"
			+ ")";

	// Table Create Statements
	// ADDRESS table create statement
	private static final String CREATE_TABLE_ADDRESS = "CREATE TABLE " + TABLE_ADDRESS + "("
			+ KEY_ID + " INTEGER PRIMARY KEY," 
			+ KEY_SERVER_ID + " INTEGER,"
			+ KEY_ADDRESS_POSTCODE + " INTEGER,"
			+ KEY_ADDRESS_CITY + " TEXT,"
			+ KEY_ADDRESS_STREET + " TEXT,"
			+ KEY_ADDRESS_STREETNUMBER + " INTEGER" + ")";
	
	public DatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
    public void onCreate(SQLiteDatabase db) {
        // creating required tables
        db.execSQL(CREATE_TABLE_CATEGORY);
        db.execSQL(CREATE_TABLE_ATTRACTION);
        db.execSQL(CREATE_TABLE_CATEGORY_ATTRACTION);
        db.execSQL(CREATE_TABLE_EVENT);
        db.execSQL(CREATE_TABLE_BUILDER);
        db.execSQL(CREATE_TABLE_EXTRAINFO);
        db.execSQL(CREATE_TABLE_IMAGE);
        db.execSQL(CREATE_TABLE_ATTRACTION_IMAGE);
        db.execSQL(CREATE_TABLE_CONTACT);
        db.execSQL(CREATE_TABLE_ADDRESS);
    }
 
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // on upgrade drop older tables
    	db.execSQL("DROP TABLE IF EXISTS " + TABLE_CATEGORY);
    	db.execSQL("DROP TABLE IF EXISTS " + TABLE_ATTRACTION);
    	db.execSQL("DROP TABLE IF EXISTS " + TABLE_CATEGORY_ATTRACTION);
    	db.execSQL("DROP TABLE IF EXISTS " + TABLE_EVENT);
    	db.execSQL("DROP TABLE IF EXISTS " + TABLE_BUILDER);
    	db.execSQL("DROP TABLE IF EXISTS " + TABLE_EXTRAINFO);
    	db.execSQL("DROP TABLE IF EXISTS " + TABLE_IMAGE);
    	db.execSQL("DROP TABLE IF EXISTS " + TABLE_ATTRACTION_IMAGE);
    	db.execSQL("DROP TABLE IF EXISTS " + TABLE_CONTACT);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ADDRESS);
        
        // create new tables
        onCreate(db);
    }
    
    /**
     * Close database connection
     */
	public void closeDB() {
	    SQLiteDatabase db = this.getReadableDatabase();
	    if (db != null && db.isOpen())
	        db.close();
	}
	
	/**
	 * Empty all database tables
	 */
	public void emptyDB(){
		SQLiteDatabase db = this.getWritableDatabase();
		
		db.delete(TABLE_CATEGORY, null, null);
        db.delete(TABLE_ATTRACTION, null, null);
        db.delete(TABLE_CATEGORY_ATTRACTION, null, null);
        db.delete(TABLE_EVENT, null, null);
        db.delete(TABLE_BUILDER, null, null);
        db.delete(TABLE_EXTRAINFO, null, null);
        db.delete(TABLE_IMAGE, null, null);
        db.delete(TABLE_ATTRACTION_IMAGE, null, null);
        db.delete(TABLE_CONTACT, null, null);
        db.delete(TABLE_ADDRESS, null, null);
	}
    
	//---------------------------------------------------------------- table CATEGORY -----------------------------------------------------------------------//
	
    /**
     * Creates new category on database from given category object
     * @param category object containing the categories data
     * @return categoryId (serverId) of the category
     */
    public int createCategory(Category category){
    	SQLiteDatabase db = this.getWritableDatabase();
    	
    	ContentValues values = new ContentValues();
    	values.put(KEY_SERVER_ID, category.getServerId());
    	values.put(KEY_CATEGORY_NAME, category.getName());
    	values.put(KEY_CATEGORY_DESCRIPTION, category.getDescription());
    	//Attractions on category are not set here (read-only); they are set by the attractions, when saved
    	
    	db.insert(TABLE_CATEGORY, null, values);
    	
    	return category.getServerId();
    }
    
	/**
	 * Gets Category by the categories serverId
	 * @param categoryId (serverId) of the attraction
	 * @return Category object to the given categoryId
	 */
	public Category getCategory(int categoryId) {
		if(categoryId==0)
			return null;
		
        SQLiteDatabase db = this.getReadableDatabase();
     
        String selectQuery = "SELECT  * FROM " + TABLE_CATEGORY + " WHERE "
                + KEY_SERVER_ID + " = " + categoryId;
     
        Log.e(LOG, selectQuery);
     
        Cursor c = db.rawQuery(selectQuery, null);
     
        if (c != null)
            c.moveToFirst();
     
        Category category = new Category();
        
        category.setCategoryId(c.getInt(c.getColumnIndex(KEY_ID)));
        category.setServerId(c.getInt(c.getColumnIndex(KEY_SERVER_ID)));
        
        category.setName(c.getString(c.getColumnIndex(KEY_CATEGORY_NAME)));
        category.setDescription(c.getString(c.getColumnIndex(KEY_CATEGORY_DESCRIPTION)));
        
        // Get linked Attractions via CATEGORY_ATTRACTION
        category.setAttractionIds(getAllAttractionsByCategory(category.getServerId()));
        
        return category;
    }
    
	/**
	 * Delivers a list of all categories as Category objects
	 * @return list of all categories as Category objects
	 */
	public List<Category> getAllCategories() {
	    List<Category> categories = new ArrayList<Category>();
	    String selectQuery = "SELECT * FROM " + TABLE_CATEGORY;
	 
	    Log.e(LOG, selectQuery);
	 
	    SQLiteDatabase db = this.getReadableDatabase();
	    Cursor c = db.rawQuery(selectQuery, null);
	 
	    // looping through all rows and adding to list
	    if (c.moveToFirst()) {
	        do {
	        	Category category = new Category();
	            
	            category.setCategoryId(c.getInt(c.getColumnIndex(KEY_ID)));
	            category.setServerId(c.getInt(c.getColumnIndex(KEY_SERVER_ID)));
	            
	            category.setName(c.getString(c.getColumnIndex(KEY_CATEGORY_NAME)));
	            category.setDescription(c.getString(c.getColumnIndex(KEY_CATEGORY_DESCRIPTION)));
	            
	            // Get linked Attractions via CATEGORY_ATTRACTION
	            category.setAttractionIds(getAllAttractionsByCategory(category.getServerId()));
	            
	            // adding to category list
	            categories.add(category);
	        } while (c.moveToNext());
	    }
        
	    return categories;
	}
	
	//-------------------------------------------------------- table CATEGORY_ATTRACTION ---------------------------------------------------------------//
	
    /**
     * Connects a attraction to a given category
     * @param categoryId (serverId) of the category
     * @param attractionId (serverId) of the attraction
     * @return categoryId (serverId) transformed to primitive int (null -> 0)
     */
	public int createCategoryAttraction(Integer categoryId, int attractionId) {
		SQLiteDatabase db = this.getWritableDatabase();
		 
		//Transform possible null value to zero
		int primitiveCategoryId = categoryId == null ? 0 : categoryId;
		
        ContentValues values = new ContentValues();
        values.put(KEY_CA_CATEGORY_ID, primitiveCategoryId);
        values.put(KEY_CA_ATTRACTION_ID, attractionId);
 
        db.insert(TABLE_CATEGORY_ATTRACTION, null, values);

        return primitiveCategoryId;
	}
	
	/**
	 * Delivers a List containing all ids of attractions connected to the given category
	 * @param categoryId (serverId) of the category
	 * @return list of attraction ids (serverId)
	 */
	public List<Integer> getAllAttractionsByCategory(int categoryId) {
		if(categoryId==0)
			return null;
		
		List<Integer> attractionIds = new ArrayList<Integer>();
		
		String selectQuery = "SELECT  * FROM " + TABLE_CATEGORY_ATTRACTION + " WHERE "
				+ KEY_CA_CATEGORY_ID + " = '" + categoryId + "'";
		
		Log.e(LOG, selectQuery);
		
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.rawQuery(selectQuery, null);
		
		// looping through all rows and adding to list
		if (c.moveToFirst()) {
			do {	 
				// adding to builder list list
				attractionIds.add(c.getInt(c.getColumnIndex(KEY_CA_ATTRACTION_ID)));
			} while (c.moveToNext());
		}

		return attractionIds;
	}
	
	//-------------------------------------------------------------- table ATTRACTION ------------------------------------------------------------------//
	
	/**
     * Creates new attraction on database from given attraction object
     * @param attraction object containing the attractions data
     * @return attractionId (serverId) of the attraction
     */
    public int createAttraction(Attraction attraction){
    	SQLiteDatabase db = this.getWritableDatabase();
    	
    	ContentValues values = new ContentValues();
    	values.put(KEY_SERVER_ID, attraction.getServerId());
    	values.put(KEY_ATTRACTION_TITLE, attraction.getTitle());
    	values.put(KEY_ATTRACTION_LONGITUDE, attraction.getLongitude());
    	values.put(KEY_ATTRACTION_LATITUDE, attraction.getLatitude());
    	values.put(KEY_ATTRACTION_PUBLISHED, attraction.isPublished());
    	values.put(KEY_ATTRACTION_SHORT_DESCRIPTION, attraction.getShortDescription());
    	values.put(KEY_ATTRACTION_DESCRIPTION, attraction.getDescription());
    	values.put(KEY_ATTRACTION_INFO, attraction.getInfo());
       	values.put(KEY_ATTRACTION_ADDRESS_ID, attraction.getAddressId());
    	values.put(KEY_ATTRACTION_EXTRAINFO_ID, attraction.getExtrainfoId());
    	values.put(KEY_ATTRACTION_BUILDER_ID, attraction.getBuilderId());
    	
    	db.insert(TABLE_ATTRACTION, null, values);
    	
    	// Create link via ATTRACTION_IMAGE
    	for(Integer imageId : attraction.getImageIds()){
    		createAttractionImage(attraction.getServerId(), imageId);
    	}
    	
    	// Create link via CATEGORY_ATTRACTION
    	for(Integer categoryId : attraction.getCategoryIds()){
    		createCategoryAttraction(categoryId, attraction.getServerId());
    	}
    	
    	return attraction.getServerId();
    }
	
	/**
	 * Gets Attraction by the attractions serverId
	 * @param attractionId (serverId) of the attraction
	 * @return Attraction object to the given attractionId
	 */
	public Attraction getAttraction(int attractionId) {
		if(attractionId==0)
			return null;
		
        SQLiteDatabase db = this.getReadableDatabase();
     
        String selectQuery = "SELECT  * FROM " + TABLE_ATTRACTION + " WHERE "
                + KEY_SERVER_ID + " = " + attractionId;
     
        Log.e(LOG, selectQuery);
     
        Cursor c = db.rawQuery(selectQuery, null);
     
        if (c != null)
            c.moveToFirst();
     
        Attraction attraction = new Attraction();
        
        attraction.setAttractionId(c.getInt(c.getColumnIndex(KEY_ID)));
        attraction.setServerId(c.getInt(c.getColumnIndex(KEY_SERVER_ID)));
        
        attraction.setTitle(c.getString(c.getColumnIndex(KEY_ATTRACTION_TITLE)));
        attraction.setLongitude(c.getFloat(c.getColumnIndex(KEY_ATTRACTION_LONGITUDE)));
        attraction.setLatitude(c.getFloat(c.getColumnIndex(KEY_ATTRACTION_LATITUDE)));
        attraction.setPublished(c.getInt(c.getColumnIndex(KEY_ATTRACTION_PUBLISHED))==1);
        attraction.setShortDescription(c.getString(c.getColumnIndex(KEY_ATTRACTION_SHORT_DESCRIPTION)));
        attraction.setDescription(c.getString(c.getColumnIndex(KEY_ATTRACTION_DESCRIPTION)));
        attraction.setInfo(c.getString(c.getColumnIndex(KEY_ATTRACTION_INFO)));

        attraction.setAddressId(c.getInt(c.getColumnIndex(KEY_ATTRACTION_ADDRESS_ID)));
        attraction.setExtrainfoId(c.getInt(c.getColumnIndex(KEY_ATTRACTION_EXTRAINFO_ID)));
        attraction.setBuilderId(c.getInt(c.getColumnIndex(KEY_ATTRACTION_BUILDER_ID)));
        
        // Get linked Images via ATTRACTION_IMAGE
        attraction.setImageIds(getAllImagesByAttraction(attraction.getServerId()));      

        //Categories on attraction are not get here (write-only); they will be read, when loading a category
        
        return attraction;
    }
	
	/**
	 * Delivers a list of all attractions as Attraction objects
	 * @return list of all attractions as Attraction objects
	 */
	public List<Attraction> getAllAttractions() {
	    List<Attraction> attractions = new ArrayList<Attraction>();
	    String selectQuery = "SELECT * FROM " + TABLE_ATTRACTION;
	 
	    Log.e(LOG, selectQuery);
	 
	    SQLiteDatabase db = this.getReadableDatabase();
	    Cursor c = db.rawQuery(selectQuery, null);
	 
	    // looping through all rows and adding to list
	    if (c.moveToFirst()) {
	        do {
	        	Attraction attraction = new Attraction();
	            
	            attraction.setAttractionId(c.getInt(c.getColumnIndex(KEY_ID)));
	            attraction.setServerId(c.getInt(c.getColumnIndex(KEY_SERVER_ID)));
	            
	            attraction.setTitle(c.getString(c.getColumnIndex(KEY_ATTRACTION_TITLE)));
	            attraction.setLongitude(c.getFloat(c.getColumnIndex(KEY_ATTRACTION_LONGITUDE)));
	            attraction.setLatitude(c.getFloat(c.getColumnIndex(KEY_ATTRACTION_LATITUDE)));
	            attraction.setPublished(c.getInt(c.getColumnIndex(KEY_ATTRACTION_PUBLISHED))==1);
	            attraction.setShortDescription(c.getString(c.getColumnIndex(KEY_ATTRACTION_SHORT_DESCRIPTION)));
	            attraction.setDescription(c.getString(c.getColumnIndex(KEY_ATTRACTION_DESCRIPTION)));
	            attraction.setInfo(c.getString(c.getColumnIndex(KEY_ATTRACTION_INFO)));

	            attraction.setAddressId(c.getInt(c.getColumnIndex(KEY_ATTRACTION_ADDRESS_ID)));
	            attraction.setExtrainfoId(c.getInt(c.getColumnIndex(KEY_ATTRACTION_EXTRAINFO_ID)));
	            attraction.setBuilderId(c.getInt(c.getColumnIndex(KEY_ATTRACTION_BUILDER_ID)));
	            
	            // Get linked Images via ATTRACTION_IMAGE
	            attraction.setImageIds(getAllImagesByAttraction(attraction.getServerId()));      

	            //Categories on attraction are not get here (write-only); they will be read, when loading a category
	            
	            // adding to category list
	            attractions.add(attraction);
	        } while (c.moveToNext());
	    }
	    
	    return attractions;
	}

	//---------------------------------------------------------- table ATTRACTION_IMAGE ------------------------------------------------------------//
	
	/**
	 * Connects a image to a given attraction
	 * @param attractionId (serverId) of the attraction
	 * @param imageId (serverId) of the image
	 * @return imageId (serverId) transformed to primitive int (null -> 0)
	 */
	public int createAttractionImage(int attractionId, Integer imageId) {
		SQLiteDatabase db = this.getWritableDatabase();
		 
		//Transform possible null value to zero
		int primitiveImageId = imageId == null ? 0 : imageId;
		
        ContentValues values = new ContentValues();
        values.put(KEY_AI_ATTRACTION_ID, attractionId);
        values.put(KEY_AI_IMAGE_ID, primitiveImageId);
 
        db.insert(TABLE_ATTRACTION_IMAGE, null, values);
 
        return primitiveImageId;
	}
	
	/**
	 * Delivers a List containing all ids of images connected to the given attraction
	 * @param attractionId (serverId) of the attraction
	 * @return list of image ids (serverId)
	 */
	public List<Integer> getAllImagesByAttraction(int attractionId) {
		if(attractionId==0)
			return null;
		
        List<Integer> imageIds = new ArrayList<Integer>();
		 
	    String selectQuery = "SELECT  * FROM " + TABLE_ATTRACTION_IMAGE + " WHERE "
	            + KEY_AI_ATTRACTION_ID + " = '" + attractionId + "'";
	 
	    Log.e(LOG, selectQuery);
	 
	    SQLiteDatabase db = this.getReadableDatabase();
	    Cursor c = db.rawQuery(selectQuery, null);
	 
	    // looping through all rows and adding to list
	    if (c.moveToFirst()) {
	        do {	 
	            // adding to builder list list
	        	imageIds.add(c.getInt(c.getColumnIndex(KEY_AI_IMAGE_ID)));
	        } while (c.moveToNext());
	    }
	 
	    return imageIds;
	}
	
	//-------------------------------------------------------------- table BUILDER ---------------------------------------------------------------------//
	
	/**
     * Creates new builder on database from given builder object
     * @param builder object containing the builders data
     * @return builderId (serverId) of the builder
     */
    public int createBuilder(Builder builder){
    	SQLiteDatabase db = this.getWritableDatabase();
    	
    	ContentValues values = new ContentValues();
    	values.put(KEY_SERVER_ID, builder.getServerId());
    	
    	values.put(KEY_BUILDER_NAME, builder.getName());
    	values.put(KEY_BUILDER_BIRTH_DATE, builder.getBirthDate().getTime());
    	values.put(KEY_BUILDER_DEATH_DATE, builder.getDeathDate().getTime());
    	
    	db.insert(TABLE_BUILDER, null, values);
    	
    	return builder.getServerId();
    }
	
	/**
	 * Gets Builder by the builders serverId
	 * @param builderId (serverId) of the attraction
	 * @return Builder object to the given builderId
	 */
	public Builder getBuilder(int builderId) {
		if(builderId==0)
			return null;
		
        SQLiteDatabase db = this.getReadableDatabase();
     
        String selectQuery = "SELECT  * FROM " + TABLE_BUILDER + " WHERE "
                + KEY_SERVER_ID + " = " + builderId;
     
        Log.e(LOG, selectQuery);
     
        Cursor c = db.rawQuery(selectQuery, null);
     
        if (c != null)
            c.moveToFirst();
     
        Builder builder = new Builder();
        
        builder.setBuilderId(c.getInt(c.getColumnIndex(KEY_ID)));
        builder.setServerId(c.getInt(c.getColumnIndex(KEY_SERVER_ID)));
        
        builder.setName(c.getString(c.getColumnIndex(KEY_BUILDER_NAME)));
        builder.setBirthDate(new Date(c.getInt(c.getColumnIndex(KEY_BUILDER_BIRTH_DATE))));
        builder.setDeathDate(new Date(c.getInt(c.getColumnIndex(KEY_BUILDER_DEATH_DATE))));
        
        return builder;
    }
	
	//-------------------------------------------------------------- table EVENT ---------------------------------------------------------------------//
	
	/**
     * Creates new event on database from given event object
     * @param event object containing the events data
     * @return eventId (serverId) of the event
     */
    public int createEvent(Event event){
    	SQLiteDatabase db = this.getWritableDatabase();
    	
    	ContentValues values = new ContentValues();
    	values.put(KEY_SERVER_ID, event.getServerId());
    	
    	values.put(KEY_EVENT_ATTRACTION_ID, event.getAttractionId());
    	values.put(KEY_EVENT_START_TIME, event.getStartTime().getTime());
    	values.put(KEY_EVENT_END_TIME, event.getEndTime().getTime());
    	values.put(KEY_EVENT_CONTENT, event.getContent());
    	
    	db.insert(TABLE_EVENT, null, values);
    	
    	return event.getServerId();
    }
	
	/**
	 * Gets Event by the events serverId
	 * @param eventId (serverId) of the event
	 * @return Event object to the given eventId
	 */
	public Event getEvent(int eventId) {
		if(eventId==0)
			return null;
		
        SQLiteDatabase db = this.getReadableDatabase();
     
        String selectQuery = "SELECT  * FROM " + TABLE_EVENT + " WHERE "
                + KEY_SERVER_ID + " = " + eventId;
     
        Log.e(LOG, selectQuery);
     
        Cursor c = db.rawQuery(selectQuery, null);
     
        if (c != null)
            c.moveToFirst();
     
        Event event = new Event();
        
        event.setEventId(c.getInt(c.getColumnIndex(KEY_ID)));
        event.setServerId(c.getInt(c.getColumnIndex(KEY_SERVER_ID)));
        
        event.setAttractionId(c.getInt(c.getColumnIndex(KEY_EVENT_ATTRACTION_ID)));
        event.setStartTime(new Date(c.getInt(c.getColumnIndex(KEY_EVENT_START_TIME))));
        event.setEndTime(new Date(c.getInt(c.getColumnIndex(KEY_EVENT_END_TIME))));
        event.setContent(c.getString(c.getColumnIndex(KEY_EVENT_CONTENT)));
        
        return event;
    }
	
	/**
	 * Delivers a list of all events as Event objects
	 * @return list of all events as Event objects
	 */
	public List<Event> getAllEvents() {
	    List<Event> events = new ArrayList<Event>();
	    String selectQuery = "SELECT * FROM " + TABLE_EVENT;
	 
	    Log.e(LOG, selectQuery);
	 
	    SQLiteDatabase db = this.getReadableDatabase();
	    Cursor c = db.rawQuery(selectQuery, null);
	 
	    // looping through all rows and adding to list
	    if (c.moveToFirst()) {
	        do {
	        	Event event = new Event();
	            
	            event.setEventId(c.getInt(c.getColumnIndex(KEY_ID)));
	            event.setServerId(c.getInt(c.getColumnIndex(KEY_SERVER_ID)));
	            
	            event.setAttractionId(c.getInt(c.getColumnIndex(KEY_EVENT_ATTRACTION_ID)));
	            event.setStartTime(new Date(c.getInt(c.getColumnIndex(KEY_EVENT_START_TIME))));
	            event.setEndTime(new Date(c.getInt(c.getColumnIndex(KEY_EVENT_END_TIME))));
	            event.setContent(c.getString(c.getColumnIndex(KEY_EVENT_CONTENT)));
	            
	            // adding to category list
	            events.add(event);
	        } while (c.moveToNext());
	    }
	    
	    return events;
	}
	
	//-------------------------------------------------------------- table EXTRAINFO ---------------------------------------------------------------------//
	
	/**
     * Creates new extrainfo on database from given extrainfo object
     * @param extrainfo object containing the extrainfos data
     * @return extrainfoId (serverId) of the extrainfo
     */
    public int createExtrainfo(Extrainfo extrainfo){
    	SQLiteDatabase db = this.getWritableDatabase();
    	
    	ContentValues values = new ContentValues();
    	values.put(KEY_SERVER_ID, extrainfo.getServerId());
    	
    	values.put(KEY_EXTRAINFO_OPENING_HOURS, extrainfo.getOpeningHours());
    	values.put(KEY_EXTRAINFO_PRICE, extrainfo.getPrice());
    	
    	db.insert(TABLE_EXTRAINFO, null, values);
    	
    	return extrainfo.getServerId();
    }
	
	/**
	 * Gets Extrainfo by the extrainfos serverId
	 * @param extrainfoId (serverId) of the extrainfo
	 * @return Extrainfo object to the given extrainfoId
	 */
	public Extrainfo getExtrainfo(int extrainfoId) {
		if(extrainfoId==0){
			return null;			
		}
		
        SQLiteDatabase db = this.getReadableDatabase();
     
        String selectQuery = "SELECT  * FROM " + TABLE_EXTRAINFO+ " WHERE "
                + KEY_SERVER_ID + " = " + extrainfoId;
     
        Log.e(LOG, selectQuery);
     
        Cursor c = db.rawQuery(selectQuery, null);
     
        if (c != null)
            c.moveToFirst();
     
        Extrainfo extrainfo = new Extrainfo();
        
        extrainfo.setExtrainfoId(c.getInt(c.getColumnIndex(KEY_ID)));
        extrainfo.setServerId(c.getInt(c.getColumnIndex(KEY_SERVER_ID)));
        
        extrainfo.setOpeningHours(c.getString(c.getColumnIndex(KEY_EXTRAINFO_OPENING_HOURS)));
        extrainfo.setPrice(c.getString(c.getColumnIndex(KEY_EXTRAINFO_PRICE)));
        
        return extrainfo;
    }
	
	//-------------------------------------------------------------- table IMAGE ---------------------------------------------------------------------//
	
	/**
     * Creates new image on database from given image object
     * @param image object containing the images data
     * @return imageId (serverId) of the image
     */
    public int createImage(Image image){
    	SQLiteDatabase db = this.getWritableDatabase();
    	
    	ContentValues values = new ContentValues();
    	values.put(KEY_SERVER_ID, image.getServerId());
    	
    	values.put(KEY_IMAGE_BLOB, image.getBlob());
    	values.put(KEY_IMAGE_PATH, image.getPath());
    	
    	db.insert(TABLE_IMAGE, null, values);
    	
    	return image.getServerId();
    }
	
	/**
	 * Gets Image by the images serverId
	 * @param imageId (serverId) of the image
	 * @return Image object to the given imageId
	 */
	public Image getImage(int imageId) {
		if(imageId==0)
			return null;
		
        SQLiteDatabase db = this.getReadableDatabase();
     
        String selectQuery = "SELECT  * FROM " + TABLE_IMAGE + " WHERE "
                + KEY_SERVER_ID + " = " + imageId;
     
        Log.e(LOG, selectQuery);
     
        Cursor c = db.rawQuery(selectQuery, null);
     
        if (c != null)
            c.moveToFirst();
     
        Image image = new Image();
        
        image.setImageId(c.getInt(c.getColumnIndex(KEY_ID)));
        image.setServerId(c.getInt(c.getColumnIndex(KEY_SERVER_ID)));
        
        image.setBlob(c.getBlob(c.getColumnIndex(KEY_IMAGE_BLOB)));
        image.setPath(c.getString(c.getColumnIndex(KEY_IMAGE_PATH)));

        return image;
    }
	
	//-------------------------------------------------------------- table ADDRESS ---------------------------------------------------------------------//
	
	/**
     * Creates new address on database from given address object
     * @param address object containing the addresss data
     * @return addressId (serverId) of the address
     */
    public int createAddress(Address address){
    	SQLiteDatabase db = this.getWritableDatabase();
    	
    	ContentValues values = new ContentValues();
    	values.put(KEY_SERVER_ID, address.getServerId());
    	
    	values.put(KEY_ADDRESS_CITY, address.getCity());
    	values.put(KEY_ADDRESS_POSTCODE, address.getPostCode());
    	values.put(KEY_ADDRESS_STREET, address.getStreet());
    	values.put(KEY_ADDRESS_STREETNUMBER, address.getStreetnumber());
    	
    	db.insert(TABLE_ADDRESS, null, values);

    	return address.getServerId();
    }
	
	/**
	 * Gets Address by the addresses serverId
	 * @param addressId (serverId) of the address
	 * @return Address object to the given addressId
	 */
	public Address getAddress(int addressId) {
		if(addressId==0)
			return null;
		
        SQLiteDatabase db = this.getReadableDatabase();
     
        String selectQuery = "SELECT  * FROM " + TABLE_ADDRESS + " WHERE "
                + KEY_SERVER_ID + " = " + addressId;
     
        Log.e(LOG, selectQuery);
     
        Cursor c = db.rawQuery(selectQuery, null);
     
        if (c != null)
            c.moveToFirst();
     
        Address address = new Address();
        
        address.setAddressId(c.getInt(c.getColumnIndex(KEY_ID)));
        address.setServerId(c.getInt(c.getColumnIndex(KEY_SERVER_ID)));
        
        address.setCity(c.getString(c.getColumnIndex(KEY_ADDRESS_CITY)));
        address.setPostCode(c.getInt(c.getColumnIndex(KEY_ADDRESS_POSTCODE)));
        address.setStreet(c.getString(c.getColumnIndex(KEY_ADDRESS_STREET)));
        address.setStreetnumber(c.getInt(c.getColumnIndex(KEY_ADDRESS_STREETNUMBER)));

        return address;
    }
	
	//-------------------------------------------------------------- table CONTACT ---------------------------------------------------------------------//
	
	/**
     * Creates new contact on database from given contact object
     * @param contact object containing the contacts data
     * @return contactId (serverId) of the contact
     */
    public int createContact(Contact contact){
    	SQLiteDatabase db = this.getWritableDatabase();
    	
    	ContentValues values = new ContentValues();
    	values.put(KEY_SERVER_ID, contact.getServerId());
    	
    	values.put(KEY_CONTACT_FIRSTNAME, contact.getFirstName());
    	values.put(KEY_CONTACT_LASTNAME, contact.getLastName());
    	values.put(KEY_CONTACT_OFFICE, contact.getOffice());
    	values.put(KEY_CONTACT_SECTION, contact.getSection());
    	values.put(KEY_CONTACT_PHONE, contact.getPhone());
    	values.put(KEY_CONTACT_FAX, contact.getFax());
    	values.put(KEY_CONTACT_EMAIL, contact.getEmail());
    	values.put(KEY_CONTACT_ADDRESS_ID, contact.getAddressId());
    	
    	db.insert(TABLE_CONTACT, null, values);
 
    	return contact.getServerId();
    }
	
	/**
	 * Gets Contact by the contact serverId
	 * @param contactId (serverId) of the contact
	 * @return Contact object to the given contactId
	 */
	public Contact getContact(int contactId) {
		if(contactId==0)
			return null;
		
        SQLiteDatabase db = this.getReadableDatabase();
     
        String selectQuery = "SELECT  * FROM " + TABLE_CONTACT + " WHERE "
                + KEY_SERVER_ID + " = " + contactId;
     
        Log.e(LOG, selectQuery);
     
        Cursor c = db.rawQuery(selectQuery, null);
     
        if (c != null)
            c.moveToFirst();
     
        Contact contact = new Contact();
        
        contact.setContactId(c.getInt(c.getColumnIndex(KEY_ID)));
        contact.setServerId(c.getInt(c.getColumnIndex(KEY_SERVER_ID)));
        
        contact.setFirstName(c.getString(c.getColumnIndex(KEY_CONTACT_FIRSTNAME)));
        contact.setLastName(c.getString(c.getColumnIndex(KEY_CONTACT_LASTNAME)));
        contact.setOffice(c.getString(c.getColumnIndex(KEY_CONTACT_OFFICE)));
        contact.setSection(c.getString(c.getColumnIndex(KEY_CONTACT_SECTION)));
        contact.setPhone(c.getString(c.getColumnIndex(KEY_CONTACT_PHONE)));
        contact.setFax(c.getString(c.getColumnIndex(KEY_CONTACT_FAX)));
        contact.setEmail(c.getString(c.getColumnIndex(KEY_CONTACT_EMAIL)));
        contact.setAddressId(c.getInt(c.getColumnIndex(KEY_CONTACT_ADDRESS_ID)));
 
        return contact;
    }
	
	/**
	 * Delivers a list of all contacts as Contact objects
	 * @return list of all contacts as Contact objects
	 */
	public List<Contact> getAllContacts() {
	    List<Contact> contacts = new ArrayList<Contact>();
	    String selectQuery = "SELECT * FROM " + TABLE_CONTACT;
	 
	    Log.e(LOG, selectQuery);
	 
	    SQLiteDatabase db = this.getReadableDatabase();
	    Cursor c = db.rawQuery(selectQuery, null);
	 
	    // looping through all rows and adding to list
	    if (c.moveToFirst()) {
	        do {
	        	Contact contact = new Contact();
	            
	            contact.setContactId(c.getInt(c.getColumnIndex(KEY_ID)));
	            contact.setServerId(c.getInt(c.getColumnIndex(KEY_SERVER_ID)));
	            
	            contact.setFirstName(c.getString(c.getColumnIndex(KEY_CONTACT_FIRSTNAME)));
	            contact.setLastName(c.getString(c.getColumnIndex(KEY_CONTACT_LASTNAME)));
	            contact.setOffice(c.getString(c.getColumnIndex(KEY_CONTACT_OFFICE)));
	            contact.setSection(c.getString(c.getColumnIndex(KEY_CONTACT_SECTION)));
	            contact.setPhone(c.getString(c.getColumnIndex(KEY_CONTACT_PHONE)));
	            contact.setFax(c.getString(c.getColumnIndex(KEY_CONTACT_FAX)));
	            contact.setEmail(c.getString(c.getColumnIndex(KEY_CONTACT_EMAIL)));
	            contact.setAddressId(c.getInt(c.getColumnIndex(KEY_CONTACT_ADDRESS_ID)));
	            
	            // adding to category list
	            contacts.add(contact);
	        } while (c.moveToNext());
	    }

	    return contacts;
	}
}
