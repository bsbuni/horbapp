package com.dhbw.horbapp.utils;

import java.util.List;

import com.dhbw.horbapp.models.Address;
import com.dhbw.horbapp.models.Attraction;
import com.dhbw.horbapp.models.Builder;
import com.dhbw.horbapp.models.Category;
import com.dhbw.horbapp.models.Contact;
import com.dhbw.horbapp.models.Event;
import com.dhbw.horbapp.models.Extrainfo;
import com.dhbw.horbapp.models.Image;

/**
 * Interface for delivery of data from either the rest service or the underlying database
 * @author bastian.blessing
 *
 */
public interface DataProvider {
	/**
	 * Returns flag of whether the Provider is for online or offline data
	 * @return true if DataProvider is in online mode; else return false
	 */
	public boolean isOnline();
	
	/**
	 * Gets all categories from service or database
	 * @return List of all categories
	 */
	public List<Category> getAllCategories();
	
	/**
	 * Gets all attractions from service or database
	 * @return List of all attractions
	 */
	public List<Attraction> getAllAttractions();
	
	/**
	 * Gets all events from service or database
	 * @return List of all events
	 */
	public List<Event> getAllEvents();
	
	/**
	 * Gets all contacts from service or database
	 * @return List of all contacts
	 */
	public List<Contact> getAllContacts();
	
	/**
	 * Gets all attractions of a given category
	 * @param categoryId of the category
	 * @return List of attractions of the category
	 */
	public List<Attraction> getAllAttractionsByCategory(int categoryId);
	
	/**
	 * Gets builder of a given attraction
	 * @param attractionId of the attraction
	 * @return builder of the attraction
	 */
	public Builder getBuilderByAttraction(int attractionId);
	
	/**
	 * Gets extrainfo of a given attraction
	 * @param attractionId of the attraction
	 * @return extrainfo of the attraction
	 */
	public Extrainfo getExtrainfoByAttraction(int attractionId);
	
	/**
	 * Gets all images of a given attraction
	 * @param attractionId of the attraction
	 * @return List of images of the attraction
	 */
	public List<Image> getAllImagesByAttraction(int attractionId);
		
	/**
	 * Gets address of a given attraction
	 * @param attractionId of the attraction
	 * @return address of the attraction
	 */
	public Address getAddressByAttraction(int attractionId);
	
	/**
	 * Gets address of a given contact
	 * @param contactId of the contact
	 * @return address of the contact
	 */
	public Address getAddressByContact(int contactId);

	/**
	 * Gets attraction of a given event
	 * @param eventId of the event
	 * @return attraction of the event
	 */
	public Attraction getAttractionByEvent(int eventId);
}
