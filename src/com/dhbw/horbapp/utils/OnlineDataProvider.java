package com.dhbw.horbapp.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;

import android.util.Log;

import com.dhbw.horbapp.models.Address;
import com.dhbw.horbapp.models.Attraction;
import com.dhbw.horbapp.models.Builder;
import com.dhbw.horbapp.models.Category;
import com.dhbw.horbapp.models.Contact;
import com.dhbw.horbapp.models.Event;
import com.dhbw.horbapp.models.Extrainfo;
import com.dhbw.horbapp.models.Image;

/**
 * Class for delivery of data from server, while app is in online mode
 * 
 * @author Bastian Blessing
 * 
 */
public class OnlineDataProvider implements DataProvider {
	// Tag for logcat
	private static final String TAG = "OnlineDataProvider";
	
	/**
	 * Constructs OnlineDataProvider if connection to service is possible
	 * @throws IOException if test connection to server failed
	 */
	public OnlineDataProvider() throws IOException{
		if(this.getAllAttractions()==null){
			//Connection not successfull
			Log.e(TAG, "Connection error: Connection test failed.");
			throw new IOException("Connection test failed.");
		}
	}	
	
	@Override
	public boolean isOnline() {
		// OnlineDataProvider is online source => true
		return true;
	}

	@Override
	public List<Category> getAllCategories() {
		try {
			List<Category> categories = RestClient.getAll(Category.class, RestClient.CATEGORIES_PATH);
		
			// 1-based index of data objects from server
			int index = 1;

			// Set index as server id, because server id is not submitted
			for (Category category : categories) {
				// FIXME Fix submit of server id on server side
				category.setServerId(index);
				index++;
			}
			
			return categories;
		} catch (IOException e) {
			// Connection error
			Log.e(TAG, "Connection error: Error on connection to service.");
			Log.e(TAG, e.getMessage(), e);
			return null;
		} catch (JSONException e) {
			// Mapping or Parsing error
			Log.e(TAG, "Json error: Mapping or parsing error.");
			Log.e(TAG, e.getMessage(), e);
			return null;
		} catch (Exception e) {
			//Unknown error
			Log.e(TAG,
					"Unknown error: Error on data retrieval from service.");
			Log.e(TAG, e.getMessage(), e);
			return null;
		}		
	}

	@Override
	public List<Attraction> getAllAttractions() {
		try {
			return RestClient.getAll(Attraction.class, RestClient.ATTRACTIONS_PATH);
		} catch (IOException e) {
			// Connection error
			Log.e(TAG, "Connection error: Error on connection to service.");
			Log.e(TAG, e.getMessage(), e);
			return null;
		} catch (JSONException e) {
			// Mapping or Parsing error
			Log.e(TAG, "Json error: Mapping or parsing error.");
			Log.e(TAG, e.getMessage(), e);
			return null;
		} catch (Exception e) {
			//Unknown error
			Log.e(TAG,
					"Unknown error: Error on data retrieval from service.");
			Log.e(TAG, e.getMessage(), e);
			return null;
		}	
	}

	@Override
	public List<Event> getAllEvents() {
		try {
			List<Event> events = RestClient.getAll(Event.class, RestClient.EVENTS_PATH);
		
			// 1-based index of data objects from server
			int index = 1;

			// Set index as server id, because server id is not submitted
			for (Event event : events) {
				// FIXME Fix submit of server id on server side
				event.setServerId(index);
				index++;
			}
			
			return events;
		} catch (IOException e) {
			// Connection error
			Log.e(TAG, "Connection error: Error on connection to service.");
			Log.e(TAG, e.getMessage(), e);
			return null;
		} catch (JSONException e) {
			// Mapping or Parsing error
			Log.e(TAG, "Json error: Mapping or parsing error.");
			Log.e(TAG, e.getMessage(), e);
			return null;
		} catch (Exception e) {
			//Unknown error
			Log.e(TAG,
					"Unknown error: Error on data retrieval from service.");
			Log.e(TAG, e.getMessage(), e);
			return null;
		}		
	}

	@Override
	public List<Contact> getAllContacts() {
		try {
			List<Contact> contacts = RestClient.getAll(Contact.class, RestClient.CONTACTS_PATH);
		
			// 1-based index of data objects from server
			int index = 1;

			// Set index as server id, because server id is not submitted
			for (Contact contact : contacts) {
				// FIXME Fix submit of server id on server side
				contact.setServerId(index);
				index++;
			}
			
			return contacts;
		} catch (IOException e) {
			// Connection error
			Log.e(TAG, "Connection error: Error on connection to service.");
			Log.e(TAG, e.getMessage(), e);
			return null;
		} catch (JSONException e) {
			// Mapping or Parsing error
			Log.e(TAG, "Json error: Mapping or parsing error.");
			Log.e(TAG, e.getMessage(), e);
			return null;
		} catch (Exception e) {
			//Unknown error
			Log.e(TAG,
					"Unknown error: Error on data retrieval from service.");
			Log.e(TAG, e.getMessage(), e);
			return null;
		}		
	}

	@Override
	public List<Attraction> getAllAttractionsByCategory(int categoryId) {
		try {
			List<Attraction> attractions = RestClient.getAll(Attraction.class, RestClient.ATTRACTIONS_PATH);
			List<Attraction> filteredAttractions = new ArrayList<Attraction>();
			
			// 1-based index of data objects from server
			int index = 1;
			
			for (Attraction attraction : attractions) {
				
				//Check whether attraction is in this category
				if(attraction.getCategoryIds().contains(categoryId)){
					// FIXME Fix submit of server id on server side
					// Set index as server id, because server id is not submitted
					attraction.setServerId(index);	
					
					//Add to filtered attractions
					filteredAttractions.add(attraction);
				}
				index++;
			}
			
			return filteredAttractions;
		} catch (IOException e) {
			// Connection error
			Log.e(TAG, "Connection error: Error on connection to service.");
			Log.e(TAG, e.getMessage(), e);
			return null;
		} catch (JSONException e) {
			// Mapping or Parsing error
			Log.e(TAG, "Json error: Mapping or parsing error.");
			Log.e(TAG, e.getMessage(), e);
			return null;
		} catch (Exception e) {
			//Unknown error
			Log.e(TAG,
					"Unknown error: Error on data retrieval from service.");
			Log.e(TAG, e.getMessage(), e);
			return null;
		}
	}

	@Override
	public Builder getBuilderByAttraction(int attractionId) {
		try {
			//Get attraction
			Attraction attraction = RestClient.getByParam(Integer.toString(attractionId), Attraction.class, RestClient.ATTRACTIONS_PATH);
			
			//Get attached builder
			int builderId = attraction.getBuilderId();
			Builder builder = RestClient.getByParam(Integer.toString(builderId), Builder.class, RestClient.BUILDERS_PATH);
			// FIXME Fix submit of server id on server side
			builder.setServerId(builderId);
			
			return builder;
		} catch (IOException e) {
			// Connection error
			Log.e(TAG, "Connection error: Error on connection to service.");
			Log.e(TAG, e.getMessage(), e);
			return null;
		} catch (Exception e) {
			//Unknown error
			Log.e(TAG,
					"Unknown error: Error on data retrieval from service.");
			Log.e(TAG, e.getMessage(), e);
			return null;
		}
	}

	@Override
	public Extrainfo getExtrainfoByAttraction(int attractionId) {
		try {
			//Get attraction
			Attraction attraction = RestClient.getByParam(Integer.toString(attractionId), Attraction.class, RestClient.ATTRACTIONS_PATH);
			
			//Get attached extrainfo
			int extrainfoId = attraction.getExtrainfoId();
			Extrainfo extrainfo = RestClient.getByParam(Integer.toString(extrainfoId), Extrainfo.class, RestClient.EXTRAINFOS_PATH);
			// FIXME Fix submit of server id on server side
			extrainfo.setServerId(extrainfoId);
			
			return extrainfo;
		} catch (IOException e) {
			// Connection error
			Log.e(TAG, "Connection error: Error on connection to service.");
			Log.e(TAG, e.getMessage(), e);
			return null;
		} catch (Exception e) {
			//Unknown error
			Log.e(TAG,
					"Unknown error: Error on data retrieval from service.");
			Log.e(TAG, e.getMessage(), e);
			return null;
		}
	}

	@Override
	public List<Image> getAllImagesByAttraction(int attractionId) {
		try {
			//Get attraction
			Attraction attraction = RestClient.getByParam(Integer.toString(attractionId), Attraction.class, RestClient.ATTRACTIONS_PATH);
			
			List<Image> images = new ArrayList<Image>();

			//FIXME Submit pictures by id from server and uncomment this section
			//Get just images of this attraction
			/*for (int imageId : attraction.getImageIds()) {
				//Get image from server
				Image image = RestClient.getByParam(Integer.toString(imageId), Image.class, RestClient.IMAGES_PATH);

				// FIXME Fix submit of server id on server side
				image.setServerId(imageId);
				RestClient.getBlobData(image);
				images.add(image);
			}*/
			
			//FIXME Delete this section if above retrieval of single picture by id from server works properly
			//Get first two images
			List<Image> unfilteredImages = RestClient.getAll(Image.class, RestClient.IMAGES_PATH);
			for(int i=0;i<2;i++){
				if(unfilteredImages.size()>i){
					Image image = unfilteredImages.get(i);
					RestClient.getBlobData(image);
					images.add(image);
				}
			}
			
			return images;
		} catch (IOException e) {
			// Connection error
			Log.e(TAG, "Connection error: Error on connection to service.");
			Log.e(TAG, e.getMessage(), e);
			return null;
		} catch (Exception e) {
			//Unknown error
			Log.e(TAG,
					"Unknown error: Error on data retrieval from service.");
			Log.e(TAG, e.getMessage(), e);
			return null;
		}
	}

	@Override
	public Address getAddressByAttraction(int attractionId) {
		try {
			//Get attraction
			Attraction attraction = RestClient.getByParam(Integer.toString(attractionId), Attraction.class, RestClient.ATTRACTIONS_PATH);
			
			//Get attached address
			int addressId = attraction.getAddressId();
			Address address = RestClient.getByParam(Integer.toString(addressId), Address.class, RestClient.ADDRESSES_PATH);
			// FIXME Fix submit of server id on server side
			address.setServerId(addressId);
			
			return address;
		} catch (IOException e) {
			// Connection error
			Log.e(TAG, "Connection error: Error on connection to service.");
			Log.e(TAG, e.getMessage(), e);
			return null;
		} catch (Exception e) {
			//Unknown error
			Log.e(TAG,
					"Unknown error: Error on data retrieval from service.");
			Log.e(TAG, e.getMessage(), e);
			return null;
		}
	}

	@Override
	public Address getAddressByContact(int contactId) {
		try {
			//Get contact
			Contact contact = RestClient.getByParam(Integer.toString(contactId), Contact.class, RestClient.CONTACTS_PATH);
			
			//Get attached address
			int addressId = contact.getAddressId();
			Address address = RestClient.getByParam(Integer.toString(addressId), Address.class, RestClient.ADDRESSES_PATH);
			// FIXME Fix submit of server id on server side
			address.setServerId(addressId);
			
			return address;
		} catch (IOException e) {
			// Connection error
			Log.e(TAG, "Connection error: Error on connection to service.");
			Log.e(TAG, e.getMessage(), e);
			return null;
		} catch (Exception e) {
			//Unknown error
			Log.e(TAG,
					"Unknown error: Error on data retrieval from service.");
			Log.e(TAG, e.getMessage(), e);
			return null;
		}
	}

	@Override
	public Attraction getAttractionByEvent(int eventId) {
		try {
			//Get event
			Event event = RestClient.getByParam(Integer.toString(eventId), Event.class, RestClient.EVENTS_PATH);
			
			//Get attached attraction
			int attractionId = event.getAttractionId();
			Attraction attraction = RestClient.getByParam(Integer.toString(attractionId), Attraction.class, RestClient.ATTRACTIONS_PATH);
			// FIXME Fix submit of server id on server side
			attraction.setServerId(attractionId);
			
			return attraction;
		} catch (IOException e) {
			// Connection error
			Log.e(TAG, "Connection error: Error on connection to service.");
			Log.e(TAG, e.getMessage(), e);
			return null;
		} catch (Exception e) {
			//Unknown error
			Log.e(TAG,
					"Unknown error: Error on data retrieval from service.");
			Log.e(TAG, e.getMessage(), e);
			return null;
		}
	}
}
