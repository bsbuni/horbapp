package com.dhbw.horbapp.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.dhbw.horbapp.models.Address;
import com.dhbw.horbapp.models.Attraction;
import com.dhbw.horbapp.models.Builder;
import com.dhbw.horbapp.models.Category;
import com.dhbw.horbapp.models.Contact;
import com.dhbw.horbapp.models.Event;
import com.dhbw.horbapp.models.Extrainfo;
import com.dhbw.horbapp.models.Image;

/**
 * Class for delivery of data from the underlying database, while app is in
 * offline mode
 * 
 * @author Bastian Blessing
 * 
 */
public class OfflineDataProvider implements DataProvider {
	// Tag for logcat
	private static final String TAG = "OfflineDataProvider";
	private static final String OFFLINE_PREFS = "offline_prefs";
	private static final String WRITE_FLAG = "write_flag";

	private DatabaseHelper db;

	/**
	 * Constructs OfflineDataProvider if data is available and complete
	 * 
	 * @param context
	 *            of program execution
	 * @throws IOException
	 *             if offline data on underlying database could be incomplete
	 */
	public OfflineDataProvider(Context context) throws IOException{	
		if(getWriteFlag(context)){
			//Application in writing mode or indicating flag is missing
			Log.e(TAG,"Database error: Offline data on underlying database is propably incomplete or not available. "
					+ "Table contents will be deleted.");
						
			//Set application to writing mode in case the flag was just missing
			setWriteFlag(context,true);
			
			//Empty all database tables
			db.emptyDB();
			
			//Set application to reading mode
			setWriteFlag(context,false);
			
			throw new IOException("Database error: Offline data on underlying database is propably incomplete or not available. "
					+ "Table contents will be deleted.");
		}
		else{			
			this.db = new DatabaseHelper(context);
		}
	}

	/**
	 * Retrieves all application data from server and writes it to the
	 * underlying database
	 * 
	 * @param context
	 *            of program execution
	 * @return true if data retrieval was successfull; false if not
	 */
	public static boolean getOfflineData(Context context) {
		try {
			List<Attraction> attractions = RestClient.getAll(Attraction.class,
					RestClient.ATTRACTIONS_PATH);
			List<Category> categories = RestClient.getAll(Category.class,
					RestClient.CATEGORIES_PATH);
			List<Event> events = RestClient.getAll(Event.class,
					RestClient.EVENTS_PATH);
			List<Contact> contacts = RestClient.getAll(Contact.class,
					RestClient.CONTACTS_PATH);
			List<Address> addresses = RestClient.getAll(Address.class,
					RestClient.ADDRESSES_PATH);
			List<Extrainfo> extrainfos = RestClient.getAll(Extrainfo.class,
					RestClient.EXTRAINFOS_PATH);
			List<Builder> builders = RestClient.getAll(Builder.class,
					RestClient.BUILDERS_PATH);
			List<Image> images = RestClient.getAll(Image.class,
					RestClient.IMAGES_PATH);

			DatabaseHelper db = new DatabaseHelper(context);

			// Set application to writing mode
			setWriteFlag(context, true);

			// Empty all database tables
			db.emptyDB();

			// Save attractions to underlying database
			for (Attraction attraction : attractions) {
				db.createAttraction(attraction);
			}

			// 1-based index of data objects from server
			int index = 1;
			// Save categories to underlying database
			for (Category category : categories) {
				// FIXME Fix submit of server id on server side
				// Set index as server id, because server id is not submitted
				category.setServerId(index);
				db.createCategory(category);
				index++;
			}

			index = 1;
			// Save events to underlying database
			for (Event event : events) {
				// FIXME Fix submit of server id on server side
				// Set index as server id, because server id is not submitted
				event.setServerId(index);
				db.createEvent(event);
				index++;
			}

			index = 1;
			// Save contacts to underlying database
			for (Contact contact : contacts) {
				// FIXME Fix submit of server id on server side
				// Set index as server id, because server id is not submitted
				contact.setServerId(index);
				db.createContact(contact);
				index++;
			}

			index = 1;
			// Save addresses to underlying database
			for (Address address : addresses) {
				// FIXME Fix submit of server id on server side
				// Set index as server id, because server id is not submitted
				address.setServerId(index);
				db.createAddress(address);
				index++;
			}

			index = 1;
			// Save extrainfos to underlying database
			for (Extrainfo extrainfo : extrainfos) {
				// FIXME Fix submit of server id on server side
				// Set index as server id, because server id is not submitted
				extrainfo.setServerId(index);
				db.createExtrainfo(extrainfo);
				index++;
			}

			index = 1;
			// Save builders to underlying database
			for (Builder builder : builders) {
				// FIXME Fix submit of server id on server side
				// Set index as server id, because server id is not submitted
				builder.setServerId(index);
				db.createBuilder(builder);
				index++;
			}

			index = 1;
			// Save images to underlying database
			for (Image image : images) {
				//Get actual image data
				RestClient.getBlobData(image);
				
				// FIXME Fix submit of server id on server side
				// Set index as server id, because server id is not submitted
				image.setServerId(index);
				db.createImage(image);
				index++;
			}

			// Set application to reading mode
			setWriteFlag(context, false);

			return true;
		} catch (IOException e) {
			// Connection error
			Log.e(TAG, "Connection error: Error on connection to service.");
			Log.e(TAG, e.getMessage(), e);
			return false;
		} catch (JSONException e) {
			// Mapping or Parsing error
			Log.e(TAG, "Json error: Mapping or parsing error.");
			Log.e(TAG, e.getMessage(), e);
			return false;
		} catch (Exception e) {
			//Unknown error
			Log.e(TAG,
					"Unknown error: Error on data retrieval from service to database.");
			Log.e(TAG, e.getMessage(), e);
			return false;
		}
	}

	/**
	 * Returns from device memory whether the application is in write state or offline data is available
	 * 
	 * @param context
	 *            of program execution
	 * @return true if in write state or write flag is not found; false if offline data is available 
	 */
	private static boolean getWriteFlag(Context context) {
		SharedPreferences sp = context.getSharedPreferences(OFFLINE_PREFS,
				Activity.MODE_PRIVATE);
		return sp.getBoolean(WRITE_FLAG, true);
	}

	/**
	 * Sets write flag on device memory to given value
	 * 
	 * @param context
	 *            of program execution
	 * @param value
	 *            to which the flag is to be set
	 */
	private static void setWriteFlag(Context context, boolean value) {
		SharedPreferences sp = context.getSharedPreferences(OFFLINE_PREFS,
				Activity.MODE_PRIVATE);
		SharedPreferences.Editor editor = sp.edit();
		editor.putBoolean(WRITE_FLAG, value);
		editor.commit();
	}

	@Override
	public boolean isOnline() {
		// OfflineDataProvider is no online source => false
		return false;
	}

	@Override
	public List<Category> getAllCategories() {
		try {
			return db.getAllCategories();
		} catch (Exception e) {
			Log.e(TAG, e.getMessage(), e);
			Log.e(TAG,
					"Database error: error on data retrieval from underlying database");
			return null;
		}
	}

	@Override
	public List<Attraction> getAllAttractions() {
		try {
			return db.getAllAttractions();
		} catch (Exception e) {
			Log.e(TAG, e.getMessage(), e);
			Log.e(TAG,
					"Database error: error on data retrieval from underlying database");
			return null;
		}
	}

	@Override
	public List<Event> getAllEvents() {
		try {
			return db.getAllEvents();
		} catch (Exception e) {
			Log.e(TAG, e.getMessage(), e);
			Log.e(TAG,
					"Database error: error on data retrieval from underlying database");
			return null;
		}
	}

	@Override
	public List<Contact> getAllContacts() {
		try {
			return db.getAllContacts();
		} catch (Exception e) {
			Log.e(TAG, e.getMessage(), e);
			Log.e(TAG,
					"Database error: error on data retrieval from underlying database");
			return null;
		}
	}

	@Override
	public List<Attraction> getAllAttractionsByCategory(int categoryId) {
		try {
			List<Integer> attractionIds = db
					.getAllAttractionsByCategory(categoryId);
			List<Attraction> attractions = new ArrayList<Attraction>();

			for (Integer attractionId : attractionIds) {
				Attraction attraction = db.getAttraction(attractionId);
				attractions.add(attraction);
			}

			return attractions;
		} catch (Exception e) {
			Log.e(TAG, e.getMessage(), e);
			Log.e(TAG,
					"Database error: error on data retrieval from underlying database");
			return null;
		}
	}

	@Override
	public Builder getBuilderByAttraction(int attractionId) {
		try {
			Attraction attraction = db.getAttraction(attractionId);
			int builderId = attraction.getBuilderId();
			return db.getBuilder(builderId);
		} catch (Exception e) {
			Log.e(TAG, e.getMessage(), e);
			Log.e(TAG,
					"Database error: error on data retrieval from underlying database");
			return null;
		}
	}

	@Override
	public Extrainfo getExtrainfoByAttraction(int attractionId) {
		try {
			Attraction attraction = db.getAttraction(attractionId);
			int extrainfoId = attraction.getExtrainfoId();
			return db.getExtrainfo(extrainfoId);
		} catch (Exception e) {
			Log.e(TAG, e.getMessage(), e);
			Log.e(TAG,
					"Database error: error on data retrieval from underlying database");
			return null;
		}
	}

	@Override
	public List<Image> getAllImagesByAttraction(int attractionId) {
		try {
			List<Integer> imageIds = db.getAllImagesByAttraction(attractionId);
			List<Image> images = new ArrayList<Image>();

			for (Integer imageId : imageIds) {
				Image image = db.getImage(imageId);
				images.add(image);
			}

			return images;
		} catch (Exception e) {
			Log.e(TAG, e.getMessage(), e);
			Log.e(TAG,
					"Database error: error on data retrieval from underlying database");
			return null;
		}
	}

	@Override
	public Address getAddressByAttraction(int attractionId) {
		try {
			Attraction attraction = db.getAttraction(attractionId);
			int addressId = attraction.getAddressId();
			return db.getAddress(addressId);
		} catch (Exception e) {
			Log.e(TAG, e.getMessage(), e);
			Log.e(TAG,
					"Database error: error on data retrieval from underlying database");
			return null;
		}
	}

	@Override
	public Address getAddressByContact(int contactId) {
		try {
			Contact contact = db.getContact(contactId);
			int addressId = contact.getAddressId();
			return db.getAddress(addressId);
		} catch (Exception e) {
			Log.e(TAG, e.getMessage(), e);
			Log.e(TAG,
					"Database error: error on data retrieval from underlying database");
			return null;
		}
	}

	@Override
	public Attraction getAttractionByEvent(int eventId) {
		try {
			Event event = db.getEvent(eventId);
			int attractionId = event.getAttractionId();
			return db.getAttraction(attractionId);
		} catch (Exception e) {
			Log.e(TAG, e.getMessage(), e);
			Log.e(TAG,
					"Database error: error on data retrieval from underlying database");
			return null;
		}
	}
}
