package com.dhbw.horbapp.utils;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.ByteArrayBuffer;
import org.json.JSONArray;
import org.json.JSONException;

import android.os.StrictMode;

import com.dhbw.horbapp.models.Image;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Class for communication to the service delivering all application data; only HTTP-GET
 * @author bastian.blessing
 *
 */
public class RestClient {
	
	private static final String BASE_URL = "http://horbapp.alexhausmann.de/api";
	private static final String FORMAT = "json";
	public static final String ADDRESSES_PATH = "addresses";
	public static final String CONTACTS_PATH = "contacts";
	public static final String ATTRACTIONS_PATH = "attractions";
	public static final String IMAGES_PATH = "images";
	public static final String BUILDERS_PATH = "builders";
	public static final String EVENTS_PATH = "events";
	public static final String CATEGORIES_PATH = "categories";
	public static final String EXTRAINFOS_PATH = "extrainfos";
	
	/**
	 * Gets a info of a given type from the server by its serverId
	 * @param param of the object
	 * @return info object to given param
	 * @throws IOException if mapping of json to objects fails
	 */
	public static <T> T getByParam(String param, Class<T> typeClass, String path) throws IOException{
		String json = doGet(BASE_URL + "/" + path + "/" + param + "?format=" + FORMAT);
		ObjectMapper mapper = new ObjectMapper(); 
		
		//Ignore properties that are not mapped
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		return mapper.readValue(json, typeClass);
	}
	
	/**
	 * Gets all infos of a given type from the server
	 * @param <T>
	 * @return List of info objects
	 * @throws JSONException if the json from the service could not be handled properly
	 * @throws IOException if mapping of json to objects fails
	 */
	public static <T> List<T> getAll(Class<T> typeClass, String path) throws JSONException, IOException{
		String thePath = BASE_URL + "/" + path + "/?format=" + FORMAT;
		String jsonArr = doGet(thePath);
		JSONArray jArr = new JSONArray(jsonArr);
		ObjectMapper mapper = new ObjectMapper(); 

		//Ignore properties that are not mapped
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		
		List<T> infos = new ArrayList<T>();
		for(int i=0; i<jArr.length(); i++){
			String json = jArr.getJSONObject(i).toString();
			infos.add(mapper.readValue(json, typeClass));
		}
		
		return infos;
	}
	
	/**
	 * Gets json from service on given url; http method = GET
	 * @param url of the service
	 * @return json string returned by the service
	 * @throws IOException if connection failed
	 * @throws ClientProtocolException if http connection didn't work properly
	 */
	private static String doGet(String url) throws ClientProtocolException, IOException {
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
		
	    String result = "";
		HttpClient httpclient = new DefaultHttpClient();
	    HttpGet httpget = new HttpGet(url);
	    httpget.addHeader("accept", "application/json");

	    // Execute the request
	    HttpResponse response;
    
        response = httpclient.execute(httpget);
        HttpEntity entity = response.getEntity();

        if (entity != null) {
            InputStream instream = entity.getContent();
            result= convertStreamToString(instream);
            instream.close();
        }
	    
	    return result;
	}
	
	/**
	 * Convert content from InputStream to String
	 * @param is InputStream containing the data
	 * @return data from InputStream as String
	 */
    private static String convertStreamToString(InputStream is) {
        
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
 
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

    /**
     * Retrieves actual blob data of the image
     * @param image of which the blob data is to be retrieved
     */
	public static void getBlobData(Image image) {
		try {
			URL imageUrl = new URL(image.getPath());

			URLConnection ucon = imageUrl.openConnection();

            InputStream is = ucon.getInputStream();
            BufferedInputStream bis = new BufferedInputStream(is);

            ByteArrayBuffer baf = new ByteArrayBuffer(500);
            int current = 0;
            while ((current = bis.read()) != -1) {
                    baf.append((byte) current);
            }

            image.setBlob(baf.toByteArray());
		} catch (Exception e) {
			//Don't try to get the image
		}		
	}
}
