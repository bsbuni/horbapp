package com.dhbw.horbapp;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Fragment showing an attractions details
 * @author Bastian Blessing
 *
 */
public class StartFragment extends Fragment {
	
	
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, 
        Bundle savedInstanceState) {
    	
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.start_fragment, container, false);
    }
}
