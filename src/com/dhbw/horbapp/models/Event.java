package com.dhbw.horbapp.models;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Object model of an event
 * @author bastian.blessing
 *
 */
public class Event implements Serializable{	
	private static final long serialVersionUID = -7766966256957130644L;
	
	private int eventId;
	private String content;

	@JsonProperty("start_time")
	private Date startTime;

	@JsonProperty("end_time")
	private Date endTime;

	@JsonProperty("attraction")
	private int attractionId;
	
	//FIXME Uncomment this section when server id is submitted
	//@JsonProperty("id")
	private int serverId;

	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public int getAttractionId() {
		return attractionId;
	}
	public void setAttractionId(int attractionId) {
		this.attractionId = attractionId;
	}
	public int getServerId() {
		return serverId;
	}
	public void setServerId(int serverId) {
		this.serverId = serverId;
	}
	public Date getStartTime() {
		return startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	public int getEventId() {
		return eventId;
	}
	public void setEventId(int eventId) {
		this.eventId = eventId;
	}

	@Override
	public String toString(){
		return this.content;
	}
}
