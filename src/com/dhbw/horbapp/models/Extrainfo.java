package com.dhbw.horbapp.models;

import java.io.Serializable;

/**
 * Object model of an extrainfo; containing additional data for attraction
 * @author bastian.blessing
 *
 */
public class Extrainfo implements Serializable{
	private static final long serialVersionUID = 4169527214728958586L;
	
	private int extrainfoId;
	private String openingHours;
	private String price;
	
	//FIXME Uncomment this section when server id is submitted
	//@JsonProperty("id")
	private int serverId;

	public String getOpeningHours() {
		return openingHours;
	}
	public void setOpeningHours(String openingHours) {
		this.openingHours = openingHours;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public int getServerId() {
		return serverId;
	}
	public void setServerId(int serverId) {
		this.serverId = serverId;
	}
	public int getExtrainfoId() {
		return extrainfoId;
	}
	public void setExtrainfoId(int extrainfoId) {
		this.extrainfoId = extrainfoId;
	}
}
