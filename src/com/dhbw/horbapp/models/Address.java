package com.dhbw.horbapp.models;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Object model of an address 
 * @author bastian.blessing
 *
 */
public class Address implements Serializable{
	private static final long serialVersionUID = -2584949715741825954L;

	private int addressId;
	
	@JsonProperty("post_code")
	private int postCode;
	private String city;
	private String street;
	
	@JsonProperty("street_number")
	private int streetnumber;
	
	//FIXME Uncomment this section when server id is submitted
	//@JsonProperty("id")
	private int serverId;
	
	public int getPostCode() {
		return postCode;
	}
	public void setPostCode(int postCode) {
		this.postCode = postCode;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public int getStreetnumber() {
		return streetnumber;
	}
	public void setStreetnumber(int streetnumber) {
		this.streetnumber = streetnumber;
	}	
	public int getServerId() {
		return serverId;
	}
	public void setServerId(int serverId) {
		this.serverId = serverId;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public int getAddressId() {
		return addressId;
	}
	public void setAddressId(int addressId) {
		this.addressId = addressId;
	}
}
