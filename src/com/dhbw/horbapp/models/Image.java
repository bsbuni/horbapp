package com.dhbw.horbapp.models;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Object model for encapsulation of an image an its additional data
 * @author bastian.blessing
 *
 */
public class Image{
	private int imageId;
	
	@JsonProperty("file")
	private String path;
	private byte[] blob;
	
	//FIXME Uncomment this section when server id is submitted
	//@JsonProperty("id")
	private int serverId;
	
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public byte[] getBlob() {
		return blob;
	}
	public void setBlob(byte[] blob) {
		this.blob = blob;
	}
	public int getServerId() {
		return serverId;
	}
	public void setServerId(int serverId) {
		this.serverId = serverId;
	}
	public int getImageId() {
		return imageId;
	}
	public void setImageId(int imageId) {
		this.imageId = imageId;
	}
}
