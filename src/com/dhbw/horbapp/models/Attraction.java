package com.dhbw.horbapp.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Object model of an attraction
 * @author bastian.blessing
 *
 */
public class Attraction implements Serializable{	
	private static final long serialVersionUID = -4346079623527119688L;
	
	private int attractionId;
	private String title;
	private float longitude;
	private float latitude;
	private boolean published;
	
	@JsonProperty("short_description")
	private String shortDescription;
	private String description;
	private String info;
	
	@JsonProperty("address")
	private int addressId;
	
	@JsonProperty("extrainfo")
	private int extrainfoId;
	
	@JsonProperty("builder")
	private int builderId;
	
	@JsonProperty("images")
	private List<Integer> imageIds = new ArrayList<Integer>();
	
	@JsonProperty("categories")
	private List<Integer> categoryIds = new ArrayList<Integer>();
	
	@JsonProperty("id")
	private int serverId;
	
	public void setBuilderId(int builderId) {
		this.builderId = builderId;
	}
	public void setImageIds(List<Integer> imageIds) {
		this.imageIds = imageIds;
	}
	public int getAddressId() {
		return addressId;
	}
	public void setAddressId(int addressId) {
		this.addressId = addressId;
	}
	public int getExtrainfoId() {
		return extrainfoId;
	}
	public void setExtrainfoId(int extrainfoId) {
		this.extrainfoId = extrainfoId;
	}
	public int getBuilderId() {
		return builderId;
	}
	public List<Integer> getImageIds() {
		return imageIds;
	}
	public int getAttractionId() {
		return attractionId;
	}
	public void setAttractionId(int id) {
		this.attractionId = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public float getLongitude() {
		return longitude;
	}
	public void setLongitude(float longitude) {
		this.longitude = longitude;
	}
	public float getLatitude() {
		return latitude;
	}
	public void setLatitude(float latitude) {
		this.latitude = latitude;
	}
	public boolean isPublished() {
		return published;
	}
	public void setPublished(boolean published) {
		this.published = published;
	}
	public String getShortDescription() {
		return shortDescription;
	}
	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getInfo() {
		return info;
	}
	public void setInfo(String info) {
		this.info = info;
	}
	public int getServerId() {
		return serverId;
	}
	public void setServerId(int serverId) {
		this.serverId = serverId;
	}
	public List<Integer> getCategoryIds() {
		return categoryIds;
	}
	public void setCategoryIds(List<Integer> categoryIds) {
		this.categoryIds = categoryIds;
	}
	
	@Override
	public String toString(){
		return this.title;
	}
}
