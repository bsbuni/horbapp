package com.dhbw.horbapp.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Object model of a category
 * @author bastian.blessing
 *
 */
public class Category implements Serializable{
	private static final long serialVersionUID = -5175822938169539656L;
	
	private int categoryId;
	private String name;
	private String description;
	
	//FIXME Uncomment this section when server id is submitted
	//@JsonProperty("id")
	private int serverId;
	private List<Integer> attractionIds = new ArrayList<Integer>();
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getServerId() {
		return serverId;
	}
	public void setServerId(int serverId) {
		this.serverId = serverId;
	}
	public List<Integer> getAttractionIds() {
		return attractionIds;
	}
	public void setAttractionIds(List<Integer> attractionIds) {
		this.attractionIds = attractionIds;
	}
	public int getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}
	
	@Override
	public String toString(){
		return this.name;
	}
}
