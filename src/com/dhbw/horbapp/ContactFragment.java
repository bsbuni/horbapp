package com.dhbw.horbapp;

import com.dhbw.horbapp.models.Address;
import com.dhbw.horbapp.models.Contact;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Fragment showing an events details
 * @author Bastian Blessing
 *
 */
public class ContactFragment extends Fragment {
	private Contact contact;
	private Address address;

	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, 
        Bundle savedInstanceState) {
    	
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.contact_fragment, container, false);
    }

    @Override
    public void onResume(){
    	super.onResume();

    	fillInData(contact, address);
    }
    
    /**
     * Fill in data into fragment; only invoke when fragment is displayed
     * @param contact that is to be displayed
     * @param address of the contact; can be null
     */
    public void fillInData(Contact contact, Address address) {
        //Save all data for later on resume
    	this.contact = contact;
    	this.address = address;
    	
    	View view = getView();
		
		if(view!=null){		
			//Show contact data
			((TextView) view.findViewById(R.id.contact_name)).setText(contact.getFirstName() + " " + contact.getLastName());
			((TextView) view.findViewById(R.id.contact_mail)).setText(contact.getEmail());
			((TextView) view.findViewById(R.id.contact_phone)).setText(contact.getPhone());
			((TextView) view.findViewById(R.id.contact_fax)).setText(contact.getFax());
			((TextView) view.findViewById(R.id.contact_office)).setText(contact.getOffice());
			((TextView) view.findViewById(R.id.contact_section)).setText(contact.getSection());
	
	    	//Show address data if available
	    	if(address!=null && address.getStreet()!=null){
	    		((TextView) view.findViewById(R.id.contact_address_street)).setText(address.getStreet() + " " + address.getStreetnumber());
	    		view.findViewById(R.id.contact_address_street_container).setVisibility(View.VISIBLE);
	    		
	    		((TextView) view.findViewById(R.id.contact_address_city)).setText(address.getPostCode() + " " + address.getCity());
	    		view.findViewById(R.id.contact_address_city_container).setVisibility(View.VISIBLE);
	    	}
		}
    }
}
